/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _Poller_h_
#define _Poller_h_ 1

#if __FreeBSD__
#include <sys/event.h>
#else // __linux__ below
#include <sys/epoll.h>
#include <sys/eventfd.h>
#include <sys/timerfd.h>
#endif

class Fibre;
template<typename> class FibreClusterGeneric;

class BasePoller {
protected:
  EventScope& eventScope;

public: // RegistrationStatus
  static const int Read  = 0x1;
  static const int Write = 0x2;

protected:
  static const int maxPoll = 1024;
#if __FreeBSD__
  struct kevent events[maxPoll];
#else // __linux__ below
  epoll_event   events[maxPoll];
#endif
  int           pollFD;
  volatile bool pollTerminate;
  volatile bool paused;

  PollerStats* stats;

  template<typename T>
  static inline void pollLoop(T& This, bool pollerFibre);

public:
  BasePoller(EventScope& es, const char* n = "BasePoller") : eventScope(es), pollTerminate(false), paused(false) {
    stats = new PollerStats(this, n);
#if __FreeBSD__
    pollFD = SYSCALLIO(kqueue());
#else // __linux__ below
    pollFD = SYSCALLIO(epoll_create1(EPOLL_CLOEXEC));
#endif
  }
  ~BasePoller() { SYSCALL(close(pollFD)); }
  EventScope& getEventScope() { return eventScope; }
  void pause() { paused = true; }

  template<bool Input, bool Output>
  void registerFD(int fd, int& status) {
    static_assert(Input || Output, "must set Read or Write in registerFD()");
#if __FreeBSD__
    struct kevent ev[2];
    int idx = 0;
    if (Input) {
      status |= Read;
      EV_SET(&ev[idx], fd, EVFILT_READ, EV_ADD | EV_CLEAR, 0, 0, 0);
      idx += 1;
    }
    if (Output) {
      status |= Write;
      EV_SET(&ev[idx], fd, EVFILT_WRITE, EV_ADD | EV_CLEAR, 0, 0, 0);
      idx += 1;
    }
    SYSCALL(kevent(pollFD, ev, idx, nullptr, 0, nullptr));
#else // __linux__ below
    bool update = status;
    if (Input) status |= Read;
    if (Output) status |= Write;
    epoll_event ev;
    ev.events = EPOLLET;
    // EPOLLERR, EPOLLHUP not actually needed?
    if (status & Read) {
      ev.events |= EPOLLIN | EPOLLRDHUP | EPOLLPRI | EPOLLERR | EPOLLHUP;
    }
    if (status & Write) {
      ev.events |= EPOLLOUT;
    }
    ev.data.fd = fd;
    SYSCALL(epoll_ctl(pollFD, update ? EPOLL_CTL_MOD : EPOLL_CTL_ADD, fd, &ev));
#endif
  }

#if 0 // unused
  void deregisterFD(int fd) {
#if __FreeBSD__
    struct kevent ev[2];
    EV_SET(&ev[0], fd, EVFILT_READ, EV_DELETE, 0, 0, 0);
    EV_SET(&ev[1], fd, EVFILT_WRITE, EV_DELETE, 0, 0, 0);
    SYSCALL(kevent(pollFD, ev, 2, nullptr, 0, nullptr));
#else // __linux__ below
    SYSCALL(epoll_ctl(pollFD, EPOLL_CTL_DEL, fd, nullptr));
#endif
  }
#endif
};

class PollerThread : public BasePoller {
  pthread_t pollThread;
  SystemSemaphore<true> pauseSem;
protected:
  PollerThread(EventScope& es, const char* n) : BasePoller(es, n) {}
  void start(void *(*loopSetup)(void*)) {
    SYSCALL(pthread_create(&pollThread, nullptr, loopSetup, this));
  }
public:
  ~PollerThread() { if (!pollTerminate) stop(); }
  pthread_t getSysID() { return pollThread; }
  inline int blockingPoll();
  void resume() { paused = false; pauseSem.V(); }
  void stop() {                    // use self-pipe trick to terminate poll loop
    pollTerminate = true;
#if __FreeBSD__
    struct kevent ev;
    EV_SET(&ev, 0, EVFILT_USER, EV_ADD | EV_ONESHOT, 0, 0, 0);
    SYSCALL(kevent(pollFD, &ev, 1, nullptr, 0, nullptr));
    EV_SET(&ev, 0, EVFILT_USER, EV_ENABLE, NOTE_TRIGGER, 0, 0);
    SYSCALL(kevent(pollFD, &ev, 1, nullptr, 0, nullptr));
    SYSCALL(pthread_join(pollThread, nullptr));
#else // __linux__ below
    int dummy = 0;
    int efd = SYSCALLIO(eventfd(0, EFD_CLOEXEC | EFD_NONBLOCK));
    registerFD<true,false>(efd, dummy);
    uint64_t val = 1;
    val = SYSCALL_EQ(write(efd, &val, sizeof(val)), sizeof(val));
    SYSCALL(pthread_join(pollThread, nullptr));
    SYSCALL(close(efd));
#endif
  }
};

class MasterPoller : public PollerThread {
  int timerFD;
  static void* pollLoopSetup(void*);
public:
  MasterPoller(EventScope& es) : PollerThread(es, "MasterPoller") {}
  void start() { PollerThread::start(pollLoopSetup); }
  ~MasterPoller() {
#if __linux__
    SYSCALL(close(timerFD));
#endif
  }
  inline int blockingPoll();

  unsigned long initTimerHandling(unsigned long fd) {
#if __FreeBSD__
    timerFD = fd;
    fd += 1;
#else
    int dummy = 0;
    timerFD = SYSCALLIO(timerfd_create(CLOCK_REALTIME, TFD_NONBLOCK | TFD_CLOEXEC));
    registerFD<true,false>(timerFD, dummy);
#endif
    return fd;
  }

  void setTimer(const Time& reltimeout) {
#if __FreeBSD__
    struct kevent ev;
    EV_SET(&ev, timerFD, EVFILT_TIMER, EV_ADD | EV_ONESHOT, NOTE_USECONDS, reltimeout.toUS(), 0);
    SYSCALL(kevent(pollFD, &ev, 1, nullptr, 0, nullptr));
#else
    itimerspec tval = { {0,0}, reltimeout };
    SYSCALL(timerfd_settime(timerFD, 0, &tval, nullptr));
#endif
  }

#if TESTING_POLLER_FIBRES
  template<bool Modify = false>
  void setupPollFD(int fd) { // (re)set up hierarchical pollling use ONESHOT
#if __FreeBSD__
    struct kevent ev;
    EV_SET(&ev, fd, EVFILT_READ, EV_ADD | EV_ONESHOT, 0, 0, 0);
    SYSCALL(kevent(pollFD, &ev, 1, nullptr, 0, nullptr));
#else // __linux__ below
    epoll_event ev;
    ev.events = EPOLLIN | EPOLLONESHOT;
    ev.data.fd = fd;
    SYSCALL(epoll_ctl(pollFD, Modify ? EPOLL_CTL_MOD : EPOLL_CTL_ADD, fd, &ev));
#endif
  }
#endif
};

#if TESTING_POLLER_FIBRES

class PollerFibre : public BasePoller {
  Fibre* pollFibre;
  FifoSemaphore<BinaryLock<>,true> pauseSem;
  static void pollLoopSetup(void*);
public:
  PollerFibre(EventScope&, FibreClusterGeneric<PollerFibre>&, size_t);
  ~PollerFibre() { if (!pollTerminate) stop(); }
  void resume() { paused = false; pauseSem.V(); }
  void stop();
  inline int blockingPoll();
};

typedef PollerFibre ClusterPoller;

#else

class ClusterPoller : public PollerThread {
  FibreClusterGeneric<ClusterPoller>& cluster;

#if TESTING_POLLER_IDLEWAIT
  size_t          pollThreshold;
  SystemLock      pollLock;
  SystemCondition pollCond;
#endif

  static void* pollLoopSetup(void*);

public:
  ClusterPoller(EventScope& es, FibreClusterGeneric<ClusterPoller>& c, size_t t) : PollerThread(es, "PollerThread"), cluster(c) {
#if TESTING_POLLER_IDLEWAIT
    pollThreshold = t;
#endif
    PollerThread::start(pollLoopSetup);
  }

  inline int blockingPoll();

#if TESTING_POLLER_IDLEWAIT
  void stop() {
    pollThreshold = 0;
    PollerThread::stop();
    pollCond.signal();
  }
#endif

  void wait(size_t count) {
#if TESTING_POLLER_IDLEWAIT
    ScopedLock<SystemLock> al(pollLock);
    if (count >= pollThreshold) return;
#if TESTING_POLLER_IDLETIMEDWAIT
    Time t;
    SYSCALL(clock_gettime(CLOCK_REALTIME, &t));
    pollCond.wait(pollLock, t + Time(0,1000000));    // 1 ms = 1,000,000 ns;
#else
    pollCond.wait(pollLock);
#endif // TESTING_POLLER_IDLETIMEDWAIT
#endif
  }

  void signal(size_t c) {
#if TESTING_POLLER_IDLEWAIT
    ScopedLock<SystemLock> al(pollLock);
    if (c == pollThreshold) pollCond.signal();
#endif
  }
};

#endif

class NoPoller {
public:
  NoPoller(EventScope&, FibreClusterGeneric<NoPoller>&, size_t) {}
  void stop() {}
  void pause() {}
  void resume() {}
#if !TESTING_POLLER_FIBRES
  void wait(size_t) {}
  void signal(size_t) {}
#endif
};

#endif /* _Poller_h_ */
