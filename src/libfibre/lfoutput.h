/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _lfoutput_h_
#define _lfoutput_h_ 1

#include <iostream>

using std::cerr;
using std::endl;

static inline void uassertprint() {}

template<typename T, typename... Args>
static inline void uassertprint(const T& t, const Args&... a) {
  cerr << ' ' << t;
  uassertprint(a...);
}

template<typename... Args>
static inline void uassertprintf(const Args&... a) {
  cerr << " -";
  uassertprint(a...);
  cerr << endl;
}

extern void _lfAbort();

#define UABORT()         { cerr << "UABORT: in " __FILE__ ":" << __LINE__ << " in " << __func__; cerr << endl;        _lfAbort(); }
#define UABORT1(msg)     { cerr << "UABORT: in " __FILE__ ":" << __LINE__ << " in " << __func__; uassertprintf(msg);  _lfAbort(); }
#define UABORTN(args...) { cerr << "UABORT: in " __FILE__ ":" << __LINE__ << " in " << __func__; uassertprintf(args); _lfAbort(); }

#if TESTING_ENABLE_ASSERTIONS
#define UCHECK(expr)           { if slowpath(!(expr)) { cerr <<  "UCHECK: " #expr " in " __FILE__ ":" << __LINE__ << " in " << __func__; cerr << endl; } }
#define UCHECK1(expr,msg)      { if slowpath(!(expr)) { cerr <<  "UCHECK: " #expr " in " __FILE__ ":" << __LINE__ << " in " << __func__; uassertprintf(msg); } }
#define UCHECKN(expr,args...)  { if slowpath(!(expr)) { cerr <<  "UCHECK: " #expr " in " __FILE__ ":" << __LINE__ << " in " << __func__; uassertprintf(args); } }
#define UASSERT(expr)          { if slowpath(!(expr)) { cerr << "UASSERT: " #expr " in " __FILE__ ":" << __LINE__ << " in " << __func__; cerr << endl;        _lfAbort(); } }
#define UASSERT1(expr,msg)     { if slowpath(!(expr)) { cerr << "UASSERT: " #expr " in " __FILE__ ":" << __LINE__ << " in " << __func__; uassertprintf(msg);  _lfAbort(); } }
#define UASSERTN(expr,args...) { if slowpath(!(expr)) { cerr << "UASSERT: " #expr " in " __FILE__ ":" << __LINE__ << " in " << __func__; uassertprintf(args); _lfAbort(); } }
#else
#define UCHECK(expr)
#define UCHECK1(expr,msg)
#define UCHECKN(expr,args...)
#define UASSERT(expr)
#define UASSERT1(expr,msg)
#define UASSERTN(expr,args...)
#endif

#endif /* _lfoutput_h_ */
