/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "runtime/RuntimeImpl.h"
#include "libfibre/Poller.h"

template<typename T>
inline void BasePoller::pollLoop(T& This, bool pollerFibre) {
  EventScope& eventScope = This.eventScope;
#if TESTING_POLLER_FIBRES
  if (pollerFibre) eventScope.registerPollFD(This.pollFD);
  else
#else
    GENASSERT(!pollerFibre);
#endif
    SystemProcessor::setupFakeContext((StackContext*)&This, _friend<BasePoller>());
  while (!This.pollTerminate) {
    int evcnt = This.blockingPoll();
    for (;;) { // drain all events with non-blocking epoll_wait
      This.stats->events.add(evcnt);
      ProcessorResumeSet procSet;
      for (int e = 0; e < evcnt; e += 1) {
#if __FreeBSD__
        struct kevent& ev = This.events[e];
        if (ev.filter == EVFILT_READ || ev.filter == EVFILT_TIMER) {
          eventScope.unblock<true>(ev.ident, procSet, _friend<BasePoller>());
        } else if (ev.filter == EVFILT_WRITE) {
          eventScope.unblock<false>(ev.ident, procSet, _friend<BasePoller>());
        }
#else // __linux__ below
        epoll_event& ev = This.events[e];
        if (ev.events & (EPOLLIN | EPOLLPRI | EPOLLERR | EPOLLHUP)) {
          eventScope.unblock<true>(ev.data.fd, procSet, _friend<BasePoller>());
        }
        if (ev.events & (EPOLLOUT | EPOLLRDHUP | EPOLLERR)) {
          eventScope.unblock<false>(ev.data.fd, procSet, _friend<BasePoller>());
        }
#endif
      }
      for (std::pair<VirtualProcessor* const,ResumeQueue>& p : procSet) {
        p.first->bulkResume(p.second, _friend<BasePoller>());
      }
      if (evcnt < maxPoll) break;
      This.stats->polls.count();
#if __FreeBSD__
      static const timespec ts = Time::zero();
      evcnt = kevent(This.pollFD, nullptr, 0, This.events, maxPoll, &ts);
#else // __linux__ below
      evcnt = epoll_wait(This.pollFD, This.events, maxPoll, 0);
#endif
      if (evcnt < 0) { GENASSERT1(errno == EINTR, errno); } // gracefully handle EINTR
    }
  }
}

inline int PollerThread::blockingPoll() {
  stats->blocks.count();
  stats->polls.count();
#if __FreeBSD__
  int evcnt = kevent(pollFD, nullptr, 0, events, maxPoll, nullptr); // blocking
#else // __linux__ below
  int evcnt = epoll_wait(pollFD, events, maxPoll, -1);              // blocking
#endif
  if (evcnt < 0) { GENASSERT1(errno == EINTR, errno); } // gracefully handle EINTR
  if (paused) pauseSem.P();
  return evcnt;
}

void* MasterPoller::pollLoopSetup(void* This) {
  pollLoop(*reinterpret_cast<MasterPoller*>(This), false);
  return nullptr;
}

inline int MasterPoller::blockingPoll() {
  if (eventScope.tryblock<true>(timerFD)) {
#if __linux__
    uint64_t count;
    if (TRY_SYSCALLIO(read(timerFD, (void*)&count, sizeof(count)), EAGAIN) < 0) goto skipTimeout;
#endif
    Time currTime;
    SYSCALL(clock_gettime(CLOCK_REALTIME, &currTime));
    eventScope.checkTimers(currTime);
  }
#if __linux__
skipTimeout:
#endif
  return PollerThread::blockingPoll();
}

#if TESTING_POLLER_FIBRES

void PollerFibre::pollLoopSetup(void* This) {
  pollLoop(*reinterpret_cast<PollerFibre*>(This), true);
}

PollerFibre::PollerFibre(EventScope& es, FibreCluster& cluster, size_t) : BasePoller(es, "PollerFibre") {
  pollFibre = new Fibre(cluster, defaultStackSize, true);
  pollFibre->setPriority(lowPriority);
  pollFibre->run(pollLoopSetup, this);
}

void PollerFibre::stop() {
  pollTerminate = true; // set termination flag, then unblock -> terminate
  eventScope.unblockPollFD(pollFD, _friend<PollerFibre>());
  eventScope.deregisterFD(pollFD);
  delete pollFibre;
}

inline int PollerFibre::blockingPoll() {
  stats->blocks.count();
  eventScope.blockPollFD(pollFD);
  stats->polls.count();
#if __FreeBSD__
  static const timespec ts = Time::zero();
  int evcnt = kevent(pollFD, nullptr, 0, events, maxPoll, &ts);
#else // __linux__ below
  int evcnt = epoll_wait(pollFD, events, maxPoll, 0);
#endif
  if (evcnt < 0) { GENASSERT1(errno == EINTR, errno); } // gracefully handle EINTR
  if (paused) pauseSem.P();
  return evcnt;
}

#else

void* ClusterPoller::pollLoopSetup(void* This) {
  pollLoop(*reinterpret_cast<ClusterPoller*>(This), false);
  return nullptr;
}

inline int ClusterPoller::blockingPoll() {
  cluster.waitForPoll();
  return PollerThread::blockingPoll();
}

#endif // TESTING_POLLER_FIBRES
