/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _lfbasics_h_
#define _lfbasics_h_ 1

#include "generic/basics.h"

#include <atomic>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/syscall.h>

// **** bootstrap object needs to come first

static class _Bootstrapper {
  static std::atomic<int> counter;
public:
  _Bootstrapper();
  ~_Bootstrapper();
} _lfBootstrap;

// **** checks and balances, using gcc/clang's 'expression statement' extension

extern void _lfAbort() __noreturn;

#if TESTING_ENABLE_ASSERTIONS
static void _SYSCALLabort() __no_inline;
static void _SYSCALLabort() { _lfAbort(); }
extern void _SYSCALLabortLock();
extern void _SYSCALLabortUnlock();
#include "syscall_macro.h"
#else
#define SYSCALL_CMP(call,cmp,expected,errcode) call
#endif

#define SYSCALL(call)            SYSCALL_CMP(call,==,0,0)
#define SYSCALL_EQ(call,val)     SYSCALL_CMP(call,==,val,0)
#define SYSCALL_GE(call,val)     SYSCALL_CMP(call,>=,val,0)
#define TRY_SYSCALL(call,code)   SYSCALL_CMP(call,==,0,code)
#define SYSCALLIO(call)          SYSCALL_CMP(call,>=,0,0)
#define TRY_SYSCALLIO(call,code) SYSCALL_CMP(call,>=,0,code)
#define uabort(msg) { std::cerr << msg << std::endl; lfAbort(); }

// **** locking

class SystemLock {
  pthread_mutex_t mutex;
  friend class SystemCondition;
public:
  SystemLock() {
    pthread_mutexattr_t attr;
    SYSCALL(pthread_mutexattr_init(&attr));
//    SYSCALL(pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_ERRORCHECK));
    SYSCALL(pthread_mutex_init(&mutex, &attr));
    SYSCALL(pthread_mutexattr_destroy(&attr));
  }
  ~SystemLock() {
    SYSCALL(pthread_mutex_destroy(&mutex));
  }
  bool tryAcquire() {
    return pthread_mutex_trylock(&mutex) == 0;
  }
  void acquire() {
    SYSCALL(pthread_mutex_lock(&mutex));
  }
  bool acquire(const Time& timeout) {
    return TRY_SYSCALL(pthread_mutex_timedlock(&mutex, &timeout), ETIMEDOUT) == 0;
  }
  void release() {
    SYSCALL(pthread_mutex_unlock(&mutex));
  }
  bool test() {
    if (!tryAcquire()) return true;
    release();
    return false;
  }
};

class SystemCondition {
  pthread_cond_t cond;
public:
  SystemCondition() {
    SYSCALL(pthread_cond_init(&cond, nullptr));
  }
  ~SystemCondition() {
    SYSCALL(pthread_cond_destroy(&cond));
  }
  void clear(SystemLock& lock) {
    SYSCALL(pthread_cond_broadcast(&cond));
    lock.release();
  }
  void wait(SystemLock& lock) {
    SYSCALL(pthread_cond_wait(&cond, &lock.mutex));
  }
  bool wait(SystemLock& lock, const Time& timeout) {
    return TRY_SYSCALL(pthread_cond_timedwait(&cond, &lock.mutex, &timeout), ETIMEDOUT) == 0;
  }
  void signal() {
    SYSCALL(pthread_cond_signal(&cond));
  }
};

class SystemLockRW {
  pthread_rwlock_t rwlock;
public:
  SystemLockRW() {
    SYSCALL(pthread_rwlock_init(&rwlock, nullptr));
  }
  ~SystemLockRW() {
    SYSCALL(pthread_rwlock_destroy(&rwlock));
  }
  void acquireRead() {
    SYSCALL(pthread_rwlock_rdlock(&rwlock));
  }
  bool acquireRead(const Time& timeout) {
    return TRY_SYSCALL(pthread_rwlock_timedrdlock(&rwlock, &timeout), ETIMEDOUT) == 0;
  }
  void acquireWrite() {
    SYSCALL(pthread_rwlock_wrlock(&rwlock));
  }
  bool acquireWrite(const Time& timeout) {
    return TRY_SYSCALL(pthread_rwlock_timedwrlock(&rwlock, &timeout), ETIMEDOUT) == 0;
  }
  void release() {
    SYSCALL(pthread_rwlock_unlock(&rwlock));
  }
  bool tryAcquireRead() {
    return pthread_rwlock_tryrdlock(&rwlock) == 0;
  }
  bool tryAcquireWrite() {
    return pthread_rwlock_trywrlock(&rwlock) == 0;
  }
};

class PlainSystemSemaphore {
  sem_t sem;
public:
  PlainSystemSemaphore(size_t c = 0) {
    SYSCALL(sem_init(&sem, 0, c));
  }
  ~PlainSystemSemaphore() {
    SYSCALL(sem_destroy(&sem));
  }
  bool empty() {
    int val;
    SYSCALL(sem_getvalue(&sem, &val));
    return val >= 0;
  }
  bool open() {
    int val;
    SYSCALL(sem_getvalue(&sem, &val));
    return val > 0;
  }
  bool tryP() {
    for (;;) {
      int ret = sem_trywait(&sem);
      if (ret == 0) return true;
      else if (errno == EAGAIN) return false;
      else { GENASSERT1(errno == EINTR, errno); }
    }
  }
  bool P() {
    while (sem_wait(&sem) < 0) { GENASSERT1(errno == EINTR, errno); }
    return true;
  }
  bool P(const Time& timeout) {
    for (;;) {
      int ret = sem_timedwait(&sem, &timeout);
      if (ret == 0) return true;
      else if (errno == ETIMEDOUT) return false;
      else { GENASSERT1(errno == EINTR, errno); }
    }
  }
  void V() {
    SYSCALL(sem_post(&sem));
  }
};

template<bool Binary>
class SystemSemaphore {
  SystemLock lock;
  SystemCondition cond;
  int counter;
  bool flag;
public:
  SystemSemaphore(size_t c = 0) : counter(c), flag(false) {}
  bool empty() { return counter >= 0; }
  bool open() { return counter > 0; }
  bool P() {
    ScopedLock<SystemLock> sl(lock);
    if (--counter < 0) {
      while (!flag) cond.wait(lock);
      flag = false;
    }
    return true;
  }
  void V() {
    ScopedLock<SystemLock> sl(lock);
    if (!Binary || counter < 1) counter += 1;
    if (counter >= 1) return;
    flag = true;
    cond.signal();
  }
};

// **** debug output

extern SystemLock* _lfDebugOutputLock;

inline void dprint() {}

template<typename T, typename... Args>
inline void dprint(T x, const Args&... a) {
  std::cerr << x;
  dprint(a...);
}

template<typename... Args>
inline void dprintl(const Args&... a) {
#if 0
  ScopedLock<SystemLock> al(*_lfDebugOutputLock);
  dprint(syscall(__NR_gettid), ' ', a..., '\n');
#endif
}

// **** system processor (here pthread) context

struct Runtime;
class StackContext;
class SystemProcessor;

// it seems noinline is needed for TLS and then volatile is free anyway...
// http://stackoverflow.com/questions/25673787/making-thread-local-variables-fully-volatile
// https://gcc.gnu.org/bugzilla/show_bug.cgi?id=66631
class Context {
protected: // definitions and initialization are in SystemProcessor.cc
  static thread_local StackContext*    volatile currStack;
  static thread_local SystemProcessor* volatile currProc;
public:
  static void setCurrStack(StackContext& s, _friend<Runtime>) __no_inline;
  static StackContext*   CurrStack() __no_inline;
  static SystemProcessor* CurrProc() __no_inline;
};

static inline StackContext* CurrStack() {
  StackContext* s = Context::CurrStack();
  GENASSERT(s);
  return s;
}

// **** global constants

#ifdef SPLIT_STACK
static const size_t defaultStackSize =  2 * pagesize<1>();
#else
static const size_t defaultStackSize = 16 * pagesize<1>();
#endif
static const size_t stackProtection = pagesize<1>();

#endif /* _lfbasics_h_ */
