/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _SystemProcessor_h_
#define _SystemProcessor_h_ 1

#include <libfibre/lfbasics.h>
#include <semaphore.h>

class BasePoller;
class Cluster;
class Fibre;

class SystemProcessor : public Context, public BaseProcessor {
  pthread_t             sysThread;
  SystemSemaphore<true> idleSem;

  bool                      terminateAck;
  FifoSemaphore<SystemLock> terminateSem;

  inline void   idleLoop();
  static void   idleLoopSetupFibre(void*);
  inline void   idleLoopRunFibre();
  static void*  idleLoopSetupPthread(void*);

protected:
  // dedicated constructor for C interface using existing pthread
  SystemProcessor(Cluster& cluster, funcvoid1_t func, ptr_t arg);

public:
  // regular constructors
  SystemProcessor();
  SystemProcessor(Cluster& cluster);

  // dedicated constructor for bootstrap
  SystemProcessor(Cluster& cluster, _friend<_Bootstrapper>);
  // dedicated initialization routine for bootstrap
  Fibre* init(_friend<_Bootstrapper>);
  // dedicated support routine to set up dummy context for poller pthreads
  static void setupFakeContext(StackContext* sc, _friend<BasePoller>);

  // destructor evicts all fibres and terminates pthread
  virtual ~SystemProcessor();

  pthread_t getSysID() { return sysThread; }

  virtual void wakeUp() {
    stats->wake.count();
    idleSem.V();
  }
};

static inline SystemProcessor& CurrProcessor() {
  SystemProcessor* proc = Context::CurrProc();
  GENASSERT(proc);
  return *proc;
}

#endif /* _SystemProcessor_h_ */
