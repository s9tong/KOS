# to automatically load this when libfibre.so is loaded:
# echo add-auto-load-safe-path DIRECTORY >> $HOME/.gdbinit
# or load via 'source DIRECTORY/libfibre.so-gdb.py'

import gdb
from contextlib import contextmanager

class FibreSupport():
    def stop_handler(event):
        if (gdb.lookup_symbol("_globalStackList")[0] == None):
            print("WARNING: no fibre debugging support - did you enable TESTING_ENABLE_DEBUGGING?")
            return
        FibreSupport.list = []
        FibreSupport.active = {}
        # save register context for later continuation
        FibreSupport.rsp = str(gdb.parse_and_eval("$rsp")).split(None, 1)[0]
        FibreSupport.rbp = str(gdb.parse_and_eval("$rbp")).split(None, 1)[0]
        FibreSupport.rip = str(gdb.parse_and_eval("$rip")).split(None, 1)[0]
        FibreSupport.currStack = str(gdb.parse_and_eval("Context::currStack"))
        # traverse runtime stack list to build internal list of fibres
        _globalStackList = gdb.parse_and_eval("_globalStackList")
        first = _globalStackList['anchor'].address
        next = _globalStackList['anchor']['link'][1]['next']
        while (next != first):
            FibreSupport.list.append(next)
            next = next['link'][1]['next']
        orig_thread = gdb.selected_thread()
        for thread in gdb.selected_inferior().threads():
            thread.switch()
            currStack = str(gdb.parse_and_eval("Context::currStack"))
            # Cache the registers for this thread, in case it represents
            # a fibre
            FibreSupport.active[currStack] = {
                    'rsp': str(gdb.parse_and_eval("$rsp")).split(None, 1)[0],
                    'rbp': str(gdb.parse_and_eval("$rbp")).split(None, 1)[0],
                    'rip': str(gdb.parse_and_eval("$rip")).split(None, 1)[0]
            }
        orig_thread.switch()

    # restore() is hooked to continue events via basic script hooks
    def restore():
        FibreSupport.prep_frame()
        # restore original register context
        gdb.execute("set $rsp = " + str(FibreSupport.rsp))
        gdb.execute("set $rbp = " + str(FibreSupport.rbp))
        gdb.execute("set $rip = " + str(FibreSupport.rip))
        gdb.execute("set Context::currStack = " + str(FibreSupport.currStack))

    def prep_frame():
        # walk stack down to innermost frame
        currframe = gdb.selected_frame()
        frame = currframe
        while (frame != gdb.newest_frame()):
            frame = frame.newer()
        frame.select()
        return currframe

    def set_fibre(arg, silent=False):
        # if current pthread: use current register context
        if (arg == gdb.parse_and_eval("Context::currStack")):
            return True
        # Check active fibre cache in case this fibre is in it
        # (FibreSupport.active is more up-to-date than
        # StackContext for retrieving stack pointers)
        argstr = str(arg)
        if (argstr in FibreSupport.active):
            rsp = FibreSupport.active[argstr]['rsp']
            rbp = FibreSupport.active[argstr]['rbp']
            rip = FibreSupport.active[argstr]['rip']
        else:
            # retrieve fibre's register context
            ftype = gdb.lookup_type('Fibre').pointer()
            ptr = gdb.Value(arg).cast(ftype)
            rsp = ptr['stackPointer']
            if (rsp == 0):
                if not silent:
                    print("cannot access stack pointer - active in different thread?")
                return False
            ptype = gdb.lookup_type('uintptr_t').pointer()
            rsp = rsp + 40            # cf. STACK_PUSH in src/generic/regsave.h
            rbp = gdb.Value(rsp).cast(ptype).dereference()
            rsp = rsp + 8
            rip = gdb.Value(rsp).cast(ptype).dereference()
        # enable fibre's register context
        gdb.execute("set $rsp = " + str(rsp))
        gdb.execute("set $rbp = " + str(rbp))
        gdb.execute("set $rip = " + str(rip))
        # set Context::currStack to point to the correct stack
        gdb.execute("set Context::currStack = " + argstr)
        return True

    def backtrace(arg):
        currframe = FibreSupport.prep_frame()
        # save register context
        tmprsp = str(gdb.parse_and_eval("$rsp")).split(None, 1)[0]
        tmprbp = str(gdb.parse_and_eval("$rbp")).split(None, 1)[0]
        tmprip = str(gdb.parse_and_eval("$rip")).split(None, 1)[0]
        # execute backtrace, if possible
        if (FibreSupport.set_fibre(arg)):
            gdb.execute("backtrace")
        # restore register context
        gdb.execute("set $rsp = " + str(tmprsp))
        gdb.execute("set $rbp = " + str(tmprbp))
        gdb.execute("set $rip = " + str(tmprip))
        # restore stack frame
        currframe.select()

    # returns frame for fibre
    @contextmanager
    def get_frame(arg):
        currframe = FibreSupport.prep_frame()
        # save register context
        tmprsp = str(gdb.parse_and_eval("$rsp")).split(None, 1)[0]
        tmprbp = str(gdb.parse_and_eval("$rbp")).split(None, 1)[0]
        tmprip = str(gdb.parse_and_eval("$rip")).split(None, 1)[0]
        tmpcurrStack = str(gdb.parse_and_eval("Context::currStack"))
        result = None
        try:
            # execute backtrace, if possible
            if (FibreSupport.set_fibre(arg, silent=True)):
                result = gdb.selected_frame()
                yield result
            else:
                yield None
        finally:
            # restore register context
            gdb.execute("set $rsp = " + str(tmprsp))
            gdb.execute("set $rbp = " + str(tmprbp))
            gdb.execute("set $rip = " + str(tmprip))
            # restore stack frame
            tmpcurrStack = gdb.execute("set Context::currStack = " + tmpcurrStack)
            currframe.select()
        return result

class InfoFibres(gdb.Command):
    """Print list of fibres"""
    def __init__(self):
        super(InfoFibres, self).__init__("info fibres", gdb.COMMAND_USER)

    def invoke(self, arg, from_tty):
        curr = gdb.parse_and_eval("Context::currStack")
        print(" Idx \tTarget\tPtr \t\t Frame")
        for i in range(len(FibreSupport.list)):
            if (FibreSupport.list[i] == curr):
                print("* ", end='')
            else:
                print("  ", end='')
            print(str(i), "\tFibre\t", str(FibreSupport.list[i]), sep='', end='')
            with FibreSupport.get_frame(FibreSupport.list[i]) as frame:
                if frame is not None:
                    # Print instruction pointer
                    print("\t", str(gdb.parse_and_eval("$rip")).split(None, 1)[0], end='')
                    print(" in ", frame.name(), sep='', end='')
                    sal = frame.find_sal()
                    if sal is not None and sal.symtab is not None:
                        print(" at ", sal.symtab.filename, ":", sal.line, sep='')
                    else:
                        print()
                else:
                    print()

class Fibre(gdb.Command):
    def __init__(self):
        super(Fibre, self).__init__("fibre", gdb.COMMAND_SUPPORT, gdb.COMPLETE_NONE, True)

class FibrePtr(gdb.Command):
    """Print backtrace of fibre at pointer (see 'info fibres')"""
    def __init__(self):
        super(FibrePtr, self).__init__("fibre ptr", gdb.COMMAND_USER)

    def invoke(self, arg, from_tty):
        FibreSupport.backtrace(gdb.parse_and_eval(arg))

class FibreIdx(gdb.Command):
    """Print backtrace of fibre at index (see 'info fibres')"""
    def __init__(self):
        super(FibreIdx, self).__init__("fibre idx", gdb.COMMAND_USER)

    def invoke(self, arg, from_tty):
        index = int(gdb.parse_and_eval(arg))
        if (index >= len(FibreSupport.list)):
            print("fibre", index, "does not exist")
            return
        FibreSupport.backtrace(FibreSupport.list[index])

class FibreSetPtr(gdb.Command):
    """Make fibre at pointer the active fibre (see 'info fibres')"""
    def __init__(self):
        super(FibreSetPtr, self).__init__("fibre setptr", gdb.COMMAND_USER)

    def invoke(self, arg, from_tty):
        FibreSupport.prep_frame()
        FibreSupport.set_fibre(gdb.parse_and_eval(arg))

class FibreSetIdx(gdb.Command):
    """Make fibre at index the active fibre (see 'info fibres')"""
    def __init__(self):
        super(FibreSetIdx, self).__init__("fibre setidx", gdb.COMMAND_USER)

    def invoke(self, arg, from_tty):
        index = int(gdb.parse_and_eval(arg))
        if (index >= len(FibreSupport.list)):
            print("fibre", index, "does not exist")
            return
        FibreSupport.prep_frame()
        FibreSupport.set_fibre(FibreSupport.list[index])

class FibreReset(gdb.Command):
    """You must use this command after 'fibre set...',
before continuing the target with 'step', 'next', 'cont', etc..."""
    def __init__(self):
        super(FibreReset, self).__init__("fibre reset", gdb.COMMAND_USER)

    def invoke(self, arg, from_tty):
        FibreSupport.restore()

FibreSupport()
InfoFibres()
Fibre()
FibrePtr()
FibreIdx()
FibreSetPtr()
FibreSetIdx()
FibreReset()
gdb.events.stop.connect(FibreSupport.stop_handler)
