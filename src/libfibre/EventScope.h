/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _EventScope_h_
#define _EventScope_h_ 1

#include "libfibre/Fibre.h"
#include "libfibre/SystemProcessor.h"

#include <unistd.h>       // close
#include <sys/resource.h> // getrlimit
#include <sys/types.h>
#include <sys/socket.h>

class EventScope {
  typedef Mutex<SystemLock>                   EventLock;
  typedef FifoSemaphore<BinaryLock<1,1>,true> EventSemaphore; // only two competitors, no backoff needed

  // A vector for FDs works well here in principle, because POSIX guarantees lowest-numbered FDs:
  // http://pubs.opengroup.org/onlinepubs/9699919799/functions/V2_chap02.html#tag_15_14
  // A fixed-size array based on 'getrlimit' is somewhat brute-force, but simple and fast.
  struct SyncIO {
    EventLock      lock;
    EventSemaphore sem;
    void reset() { lock.acquire(); sem.reset(); lock.release(); }
  };

  struct SyncRW {
    SyncIO RD;
    SyncIO WR;
    int status;
    SyncRW() : status(0) {}
  } *fdSyncVector;

  int fdcount;

  // file operations are not considered blocking in terms of select/poll/epoll
  // therefore, all file operations are executed on dedicated Cluster/SP(s)
  DiskCluster     diskCluster;
  SystemProcessor diskProcessor;

  TimerQueue      timerQueue;

  MasterPoller    masterPoller; // runs without cluster

public:
  EventScope() : fdSyncVector(0), diskCluster(*this, 0), diskProcessor(diskCluster), masterPoller(*this) {
    struct rlimit rl;
    SYSCALL(getrlimit(RLIMIT_NOFILE, &rl));  // get hard limit for file descriptor count
    fdcount = masterPoller.initTimerHandling(rl.rlim_max);
    fdSyncVector = new SyncRW[fdcount];      // create vector of R and W sync points
    masterPoller.start();
  }

  ~EventScope() {
    delete[] fdSyncVector;
  }

  void setTimer(const Time& timeout) {
    masterPoller.setTimer(timeout);
  }

  TimerQueue& getTimerQueue() {
    return timerQueue;
  }

  void checkTimers(const Time& currTime) {
    Time newTime;
    if (timerQueue.checkExpiry(currTime, newTime)) setTimer(newTime);
  }

  template<bool Input, bool Output, bool Lazy>
  void registerFD(int fd) {
#if TESTING_LAZY_FD_REGISTRATION
    if (!Lazy) return;
#endif
    GENASSERT(fd >= 0 && fd < fdcount);
    CurrPoller().registerFD<Input,Output>(fd, fdSyncVector[fd].status);
  }

  void deregisterFD(int fd) {
    GENASSERT(fd >= 0 && fd < fdcount);
    fdSyncVector[fd].status = 0;
    fdSyncVector[fd].RD.reset();
    fdSyncVector[fd].WR.reset();
  }

#if TESTING_POLLER_FIBRES
  void registerPollFD(int fd) {
    GENASSERT(fd >= 0 && fd < fdcount);
    masterPoller.setupPollFD<false>(fd); // set using ONESHOT to reduce polling
  }
  void blockPollFD(int fd) {
    GENASSERT(fd >= 0 && fd < fdcount);
    masterPoller.setupPollFD<true>(fd);  // reset using ONESHOT to reduce polling
    fdSyncVector[fd].RD.sem.P();
  }
  void unblockPollFD(int fd, _friend<ClusterPoller>) {
    GENASSERT(fd >= 0 && fd < fdcount);
    fdSyncVector[fd].RD.sem.V();
  }
#endif

  void suspendFD(int fd) {
    GENASSERT(fd >= 0 && fd < fdcount);
    fdSyncVector[fd].RD.sem.P_fake();
    fdSyncVector[fd].WR.sem.P_fake();
  }

  void resumeFD(int fd) {
    GENASSERT(fd >= 0 && fd < fdcount);
    fdSyncVector[fd].RD.sem.V();
    fdSyncVector[fd].WR.sem.V();
  }

  template<bool Input>
  void block(int fd) {
    GENASSERT(fd >= 0 && fd < fdcount);
    SyncIO& sync = Input ? fdSyncVector[fd].RD : fdSyncVector[fd].WR;
    sync.sem.P();
  }

  template<bool Input>
  bool tryblock(int fd) {
    GENASSERT(fd >= 0 && fd < fdcount);
    SyncIO& sync = Input ? fdSyncVector[fd].RD : fdSyncVector[fd].WR;
    return sync.sem.tryP();
  }

  template<bool Input>
  void unblock(int fd, ProcessorResumeSet& procSet, _friend<BasePoller>) {
    GENASSERT(fd >= 0 && fd < fdcount);
    SyncIO& sync = Input ? fdSyncVector[fd].RD : fdSyncVector[fd].WR;
#if TESTING_BULK_RESUME
    sync.sem.V_bulk(procSet);
#else
    sync.sem.V();
#endif
  }

  template<typename T, class... Args>
  T directIO(T (*diskfunc)(Args...), Args... a) {
    VirtualProcessor& proc = Fibre::migrateSelf(diskCluster, _friend<EventScope>());
    int result = diskfunc(a...);
    Fibre::migrateSelf(proc, _friend<EventScope>());
    return result;
  }

  template<bool Input, bool Yield, bool Lock, typename T, class... Args>
  T syncIO( T (*iofunc)(int, Args...), int fd, Args... a) {
    GENASSERTN(fd >= 0 && fd < fdcount, fd, fdcount);
    SyncIO& sync = Input ? fdSyncVector[fd].RD : fdSyncVector[fd].WR;
    if (Yield) Fibre::yield();
    if (Lock) sync.lock.acquire();
    for (;;) {
      T ret = iofunc(fd, a...);
      if (ret >= 0 || errno != EAGAIN) {
        if (Lock) sync.lock.release();
        return ret;
      }
#if TESTING_LAZY_FD_REGISTRATION
      bool registered;
      if (Input) registered = (fdSyncVector[fd].status & BasePoller::Read);
      else       registered = (fdSyncVector[fd].status & BasePoller::Write);
      if (!registered) registerFD<Input,!Input,true>(fd);
#endif
      sync.sem.P();
    }
  }
};

// input: yield before network read
template<typename T, class... Args>
T lfInput( T (*readfunc)(int, Args...), int fd, Args... a) {
  return CurrEventScope().syncIO<true,true,true>(readfunc, fd, a...);
}

// output: no yield before write
template<typename T, class... Args>
T lfOutput( T (*writefunc)(int, Args...), int fd, Args... a) {
  return CurrEventScope().syncIO<false,false,true>(writefunc, fd, a...);
}

// direct I/O
template<typename T, class... Args>
T lfDirectIO( T (*diskfunc)(int, Args...), int fd, Args... a) {
  return CurrEventScope().directIO(diskfunc, fd, a...);
}

// socket creation: do not register SOCK_STREAM yet (cf. listen, connect) -> mandatory for FreeBSD!
static inline int lfSocket(int domain, int type, int protocol) {
  int ret = socket(domain, type | SOCK_NONBLOCK, protocol);
  if (type != SOCK_STREAM)
  if (ret >= 0 && type != SOCK_STREAM) CurrEventScope().registerFD<true,true,false>(ret);
  return ret;
}

// POSIX says that bind might fail with EINPROGRESS (but not on Linux)
static inline int lfBind(int fd, const sockaddr *addr, socklen_t addrlen) {
  int ret = bind(fd, addr, addrlen);
  if (ret < 0 && errno == EINPROGRESS) {
    CurrEventScope().registerFD<true,false,true>(fd);
    CurrEventScope().block<true>(fd);
    socklen_t sz = sizeof(ret);
    SYSCALL(getsockopt(fd, SOL_SOCKET, SO_ERROR, &ret, &sz));
  }
  return ret;
}

// register SOCK_STREAM server fd only after 'listen' system call (cf. socket/connect)
static inline int lfListen(int fd, int backlog) {
  int ret = listen(fd, backlog);
  if (ret >= 0) CurrEventScope().registerFD<true,false,false>(fd);
  return ret;
}

// nonblocking accept for accept draining: register new file descriptor for I/O events
static inline int lfTryAccept(int fd, sockaddr *addr, socklen_t *addrlen, int flags = 0) {
  int ret = accept4(fd, addr, addrlen, flags | SOCK_NONBLOCK);
  if (ret >= 0) CurrEventScope().registerFD<true,true,false>(ret);
  return ret;
}

// accept: register new file descriptor for I/O events, no yield before accept
static inline int lfAccept(int fd, sockaddr *addr, socklen_t *addrlen, int flags = 0) {
  int ret = CurrEventScope().syncIO<true,false,true>(accept4, fd, addr, addrlen, flags | SOCK_NONBLOCK);
  if (ret >= 0) CurrEventScope().registerFD<true,true,false>(ret);
  return ret;
}

// see man 3 connect for EINPROGRESS; register SOCK_STREAM fd now (cf. socket/listen)
static inline int lfConnect(int fd, const sockaddr *addr, socklen_t addrlen) {
  CurrEventScope().registerFD<true,true,false>(fd);
  int ret = connect(fd, addr, addrlen);
  if (ret < 0 && errno == EINPROGRESS) {
    CurrEventScope().registerFD<false,true,true>(fd);
    CurrEventScope().block<false>(fd);
    socklen_t sz = sizeof(ret);
    SYSCALL(getsockopt(fd, SOL_SOCKET, SO_ERROR, &ret, &sz));
  }
  return ret;
}

// dup: duplicate file descriptor -> not necessarily a good idea (on Linux?) - think twice about it!
static inline int lfDup(int fd) {
  int ret = dup(fd);
  if (ret >= 0) CurrEventScope().registerFD<true,true,false>(ret);
  return ret;
}

static inline int lfClose(int fd) {
  CurrEventScope().deregisterFD(fd);
  return close(fd);
}

#endif /* _EventScope_h_ */
