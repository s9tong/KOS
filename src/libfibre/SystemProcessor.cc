/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "runtime/RuntimeImpl.h"
#include "libfibre/SystemProcessor.h"
#include "libfibre/FibreCluster.h"

#include <limits.h>       // PTHREAD_STACK_MIN

// instance definitions for Context members
thread_local StackContext*    volatile Context::currStack   = nullptr; // lfbasics.h
thread_local SystemProcessor* volatile Context::currProc    = nullptr; // lfbasics.h

// noinline routines for Context
void Context::setCurrStack(StackContext& s, _friend<Runtime>) { currStack = &s; }
StackContext*    Context::CurrStack()   { return currStack; }
SystemProcessor* Context::CurrProc()    { return currProc; }

SystemProcessor::SystemProcessor(Cluster& cluster, funcvoid1_t func, ptr_t arg) : BaseProcessor(cluster), terminateAck(false) {
  sysThread = pthread_self();
  if (func) {
    Fibre* tmpFibre = new Fibre(*this, defaultStackSize, _friend<SystemProcessor>());
    tmpFibre->setPriority(topPriority); // tmp fibre must run first
    tmpFibre->setAffinity(true);        // tmp fibre must run on this SP
    tmpFibre->run(func, arg);
  }
  idleLoopRunFibre();
}

SystemProcessor::SystemProcessor(Cluster& cluster) : BaseProcessor(cluster), terminateAck(false) {
  pthread_attr_t attr;                 // create pthread running idleLoop
  SYSCALL(pthread_attr_init(&attr));
  SYSCALL(pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED));
#if __linux__ // FreeBSD jemalloc segfaults when trying to use minimum stack
  SYSCALL(pthread_attr_setstacksize(&attr, PTHREAD_STACK_MIN));
#endif
  SYSCALL(pthread_create(&sysThread, &attr, idleLoopSetupPthread, this));
  SYSCALL(pthread_attr_destroy(&attr));
}

SystemProcessor::SystemProcessor() : SystemProcessor(CurrCluster()) {}

SystemProcessor::SystemProcessor(Cluster& cluster, _friend<_Bootstrapper>) : BaseProcessor(cluster), terminateAck(false) {
  sysThread = pthread_self();          // main pthread runs main routine
  Context::currProc = this;
}

Fibre* SystemProcessor::init(_friend<_Bootstrapper>) {
  // create proper idle loop fibre
  setupIdle(new Fibre(*this, defaultStackSize, _friend<SystemProcessor>()));
  idleStack->setup((ptr_t)idleLoopSetupFibre, this);
  // main fibre takes over pthread stack - create fibre without stack
  Fibre* mainFibre = new Fibre(*this, _friend<SystemProcessor>());
  Context::currStack = mainFibre;
  return mainFibre;
}

void SystemProcessor::setupFakeContext(StackContext* sc, _friend<BasePoller>) {
  Context::currProc = nullptr;
  Context::currStack = sc;
}

SystemProcessor::~SystemProcessor() {
  terminate = true;                 // no more work added, idle terminating
  wakeUp();                         // wake up idle loop (just in case)
  terminateSem.P();                 // wait for acknowledgement from idle loop
  if (this == &CurrProcessor()) {   // make sure current fibre is off this processor
    Fibre::migrateSelf(cluster);
    GENASSERT(this != &CurrProcessor());
  }
  wakeUp();                         // wake up idle loop (just in case)
  delete (Fibre*)idleStack;         // wait for idle loop to finish
}

inline void SystemProcessor::idleLoop() {
  for (;;) {
    if (terminate) {
      if (!terminateAck) {
        terminateSem.V();
        terminateAck = true;
      }
      if (empty()) {
        cluster.removeProcessor(*this);
        return;
      }
    }
    if (findWork()) continue;
    if (reinterpret_cast<FibreCluster&>(cluster).setProcessorIdle(*this, terminate)) {
      stats->idle.count();
      idleSem.P();
    }
    cluster.setProcessorBusy(*this);
  }
}

void SystemProcessor::idleLoopSetupFibre(void* sp) {
  reinterpret_cast<SystemProcessor*>(sp)->idleLoop();
}

inline void SystemProcessor::idleLoopRunFibre() {
  Context::currProc = this;
  // idle loop takes over pthread stack - create fibre without stack
  setupIdle(new Fibre(*this, _friend<SystemProcessor>()));
  Context::currStack = idleStack;
  ((Fibre*)idleStack)->runDirect(idleLoopSetupFibre, this, _friend<SystemProcessor>());
}

void* SystemProcessor::idleLoopSetupPthread(void* sp) {
  reinterpret_cast<SystemProcessor*>(sp)->idleLoopRunFibre();
  return nullptr;
}
