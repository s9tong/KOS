/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _FibreCluster_h_
#define _FibreCluster_h_ 1

#include "runtime/Cluster.h"
#include "libfibre/Poller.h"
#include "libfibre/SystemProcessor.h"

#include <semaphore.h>

static inline EventScope& CurrEventScope();

template<typename P = ClusterPoller>
class FibreClusterGeneric : public Cluster {
  volatile bool         paused;
  SystemSemaphore<true> pauseWaitSem;
  SystemSemaphore<true> pauseContSem;

  P poller;

public:
  FibreClusterGeneric(EventScope& es, size_t t = 1)
  : paused(false), pauseContSem(1), poller(es, *this, t) {}
  FibreClusterGeneric(size_t t = 1)
  : FibreClusterGeneric(CurrEventScope(), t) {}

  P& getPoller() { return poller; }
  void stopPoller() { poller.stop(); }

  void pause() {
    poller.pause();
    pauseContSem.P();
    paused = true;
    idleLock.acquire();
    BaseProcessor* proc = idleList.front();
    while (proc != idleList.edge()) {
      proc->wakeUp();
      proc = ProcessorList::next(*proc);
    }
    idleLock.release();
    size_t start = (&CurrProcessor().getCluster() == this) ? 1 : 0;
    for (size_t i = start; i < procCount(); i += 1) pauseWaitSem.P();
  }

  void resume() {
    paused = false;
    pauseContSem.V();
    poller.resume();
  }

  size_t setProcessorIdle(BaseProcessor& proc, bool terminate) {
    if (paused) {
      pauseWaitSem.V();
      pauseContSem.P();
      pauseContSem.V();
    }
    size_t c = Cluster::setProcessorIdle(proc, terminate);
#if !TESTING_POLLER_FIBRES
    poller.signal(c);
#endif
    return c;
  }

#if !TESTING_POLLER_FIBRES
  void waitForPoll() {
    poller.wait(idleCount);
  }
#endif
};

typedef FibreClusterGeneric<ClusterPoller> FibreCluster;
typedef FibreClusterGeneric<NoPoller>      DiskCluster;

static inline FibreCluster& CurrCluster() {
  return reinterpret_cast<FibreCluster&>(CurrProcessor().getCluster());
}

static inline ClusterPoller& CurrPoller() {
  return CurrCluster().getPoller();
}

static inline EventScope& CurrEventScope() {
  return CurrPoller().getEventScope();
}

#endif /* _FibreCluster_h_ */
