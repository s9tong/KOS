/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "runtime/RuntimeImpl.h"

int InitProcess() {
  Process::pcbStore = knew<Process::PCBStore>(1);
  knew<Process>()->exec("systest");
  knew<Process>()->exec("output");
  mword pidfloat = knew<Process>()->exec("float");
  Process::kill(pidfloat, SIGTEST);
  int result;
  Process::join(pidfloat, result);
#if !TESTING_KEYCODE_LOOP
  knew<Process>()->exec("kbloop");
#endif
  mword pid = knew<Process>()->exec("threadtest");
  Process::kill(pid, SIGTEST);
  knew<Process>()->exec("manythread");
#if TESTING_MEMORY_HOG
  knew<Process>()->exec("memoryhog");
#endif
  return 0;
}
