/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _bimanip_h_
#define _bimanip_h_ 1

#include "generic/basics.h"

#define __kos_builtin(x, func) \
  ( sizeof(x) <= sizeof(int)       ? __builtin_ ## func    (x) : \
    sizeof(x) <= sizeof(long)      ? __builtin_ ## func ## l(x) : \
    sizeof(x) <= sizeof(long long) ? __builtin_ ## func ## ll(x) : \
    sizeof(x) * charbits )

template <typename T>
static inline constexpr int lsbcond(T x, T alt = bitsize<T>()) {
  return x == 0 ? alt : __kos_builtin(x, ctz);
}

template <typename T>
static inline constexpr int msbcond(T x, T alt = bitsize<T>()) {
  return x == 0 ? alt : (bitsize<T>() - 1) - __kos_builtin(x, clz);
}

template <typename T>
static inline constexpr int lsb(T x) {
  return __kos_builtin(x, ctz);
}

template <typename T>
static inline constexpr int msb(T x) {
  return (bitsize<T>() - 1) - __kos_builtin(x, clz);
}

template <typename T>
static inline constexpr int popcount(T x) {
  return __kos_builtin(x, popcount);
}

template <typename T>
static inline constexpr int floorlog2( T x ) {
  return msbcond(x, bitsize<T>());
}

template <typename T>
static inline constexpr int ceilinglog2( T x ) {
  return msbcond(x - 1, limit<T>()) + 1; // x = 1 -> msb = -1 (alt) -> result is 0
}

template <typename T>
static inline constexpr int alignment( T x ) {
  return lsbcond(x, bitsize<T>());
}

#if defined(__x86_64__)

// depending on the overall code complexity, the loop can be unrolled at -O3
// "=&r"(scan) to mark as 'earlyclobber': modified before all input processed
// "+r"(newmask) to keep newmask = mask
template<size_t N, bool FindNext = false>
static inline mword multiscan(const mword* data, size_t idx = 0, bool findset = true) {

  mword result = 0;
  mword mask = ~mword(0);
  mword newmask = mask;

  for (size_t i = FindNext ? 0 : (idx / bitsize<mword>()); i < N; i += 1) {
    mword scan;
    mword datafield = (findset ? data[i] : ~data[i]);
    if (FindNext && (i == idx / bitsize<mword>())) datafield &= ~bitmask<mword>(idx % bitsize<mword>());
    asm volatile("\
      bsfq %2, %0\n\t\
      cmovzq %3, %0\n\t\
      cmovnzq %4, %1"
    : "=&r"(scan), "+r"(newmask)
    : "rm"(datafield), "r"(bitsize<mword>()), "r"(mword(0))
    : "cc");
    result += scan & mask;
    mask = newmask;
  }

  return result;
}

template<size_t N>
static inline mword multiscan_next(const mword* data, size_t idx = 0, bool findset = true) {
  return multiscan<N,true>(data, idx, findset);
}

// depending on the overall code complexity, the loop can be unrolled at -O3
// "=&r"(scan) to mark as 'earlyclobber': modified before all input processed
// "+r"(newmask) to keep newmask = mask
template<size_t N>
static inline mword multiscan_rev(const mword* data, bool findset = true) {
  mword result = 0;
  mword mask = ~mword(0);
  mword newmask = mask;
  size_t i = N;
  do {
    i -= 1;
    mword scan;
    mword datafield = (findset ? data[i] : ~data[i]);
    asm volatile("\
      bsrq %2, %0\n\t\
      cmovzq %3, %0\n\t\
      cmovnzq %4, %1"
    : "=&r"(scan), "+r"(newmask)
    : "rm"(datafield), "r"(mword(0)), "r"(mword(0))
    : "cc");
    result += (scan & mask) + (bitsize<mword>() & ~mask);
    mask = newmask;
  } while (i != 0);
  return result;
}

#else
#error unsupported architecture: only __x86_64__ supported at this time
#endif

#endif /* _bimanip_h_ */
