#ifndef _lwipopts_h_
#define _lwipopts_h_

// see opt.h for these and further options, especially for memory and sizing

#define NO_SYS                          0

#define MEM_LIBC_MALLOC									1
#define MEM_ALIGNMENT										8

#define LWIP_HAVE_LOOPIF              	1
#define LWIP_NETIF_LOOPBACK             1

#define LWIP_DHCP                     	1
#define LWIP_DNS                        1

#define LWIP_DEBUG                      LWIP_DBG_ON
#define NETIF_DEBUG                     LWIP_DBG_ON
#define TCP_DEBUG                       LWIP_DBG_ON
#define DNS_DEBUG                       LWIP_DBG_ON

#endif /* _lwipopts_h_ */
