extern "C" {
#include "lwip/sys.h"
}

#include "generic/Buffers.h"
#include "runtime/MessageQueue.h"
#include "runtime/RuntimeImpl.h"
#include "kernel/KernelHeap.h"
#include "kernel/Thread.h"

// A lock that serializes all LWIP code
static KernelOwnerLock* lwipLock = nullptr;

typedef FifoSemaphore<SystemLock,true>  BinarySemaphore;

extern "C" void sys_init(void) {
  lwipLock = knew<KernelOwnerLock>();
}

static inline BinarySemaphore* S(sys_sem_t *sem) { return reinterpret_cast<BinarySemaphore*>(*sem); }

extern "C" err_t sys_sem_new(sys_sem_t *sem, u8_t count) {
  *sem = knew<BinarySemaphore>(count);
  return ERR_OK;
}

extern "C" void sys_sem_free(sys_sem_t *sem) {
  kdelete(S(sem));
}

extern "C" void sys_sem_signal(sys_sem_t *sem) {
  S(sem)->V();
}

extern "C" u32_t sys_arch_sem_wait(sys_sem_t *sem, u32_t timeout) {
  u32_t before = kernelClock.now().toMS();
  if (timeout == 0) {
    S(sem)->P();
    return kernelClock.now().toMS() - before;
  } else if (S(sem)->P(Time::fromMS(timeout))) {
    return kernelClock.now().toMS() - before;
  } else {
    return SYS_ARCH_TIMEOUT;
  }
}

extern "C" int sys_sem_valid(sys_sem_t *sem) {
  return *sem != nullptr;
}

extern "C" void sys_sem_set_invalid(sys_sem_t *sem) {
  *sem = nullptr;
}

typedef Mutex<KernelLock> LwipMutex;
static inline LwipMutex* M(sys_mutex_t *mutex) { return reinterpret_cast<LwipMutex*>(*mutex); }

extern "C" err_t sys_mutex_new(sys_mutex_t *mutex) {
  *mutex = knew<LwipMutex>();
  return ERR_OK;
}

extern "C" void sys_mutex_free(sys_mutex_t *mutex) {
  kdelete(M(mutex));
}

extern "C" void sys_mutex_lock(sys_mutex_t *mutex) {
  M(mutex)->acquire();
}

extern "C" void sys_mutex_unlock(sys_mutex_t *mutex) {
  M(mutex)->release();
}

extern "C" int sys_mutex_valid(sys_mutex_t *mutex) {
  return *mutex != nullptr;
}

extern "C" void sys_mutex_set_invalid(sys_mutex_t *mutex) {
  *mutex = nullptr;
}

typedef MessageQueue<RuntimeRingBuffer<void*,KernelAllocator<void*>>,KernelLock> LwipMsgQueue;
static inline LwipMsgQueue* MQ(sys_mbox_t *mbox) { return reinterpret_cast<LwipMsgQueue*>(*mbox); }

extern "C" err_t sys_mbox_new(sys_mbox_t *mbox, int size) {
  *mbox = knew<LwipMsgQueue>( std::max(size,128) );
  return ERR_OK;
}

extern "C" void sys_mbox_free(sys_mbox_t *mbox) {
  kdelete(MQ(mbox));
}

extern "C" void sys_mbox_post(sys_mbox_t *mbox, void *msg) {
  reinterpret_cast<LwipMsgQueue*>(*mbox)->send(msg);
}

extern "C" err_t sys_mbox_trypost(sys_mbox_t *mbox, void *msg) {
  if (MQ(mbox)->trySend(msg)) return ERR_OK;
  return ERR_MEM;
}

extern "C" u32_t sys_arch_mbox_fetch(sys_mbox_t *mbox, void **msg, u32_t timeout) {
  uint32_t before = kernelClock.now().toMS();
  if (timeout == 0) {
    MQ(mbox)->recv(*msg);
    return kernelClock.now().toMS() - before;
  } else if (MQ(mbox)->recv(*msg, Time::fromMS(timeout))) {
    return kernelClock.now().toMS() - before;
  } else {
    return SYS_ARCH_TIMEOUT;
  }
}

extern "C" u32_t sys_arch_mbox_tryfetch(sys_mbox_t *mbox, void **msg) {
  uint32_t before = kernelClock.now().toMS();
  if (MQ(mbox)->tryRecv(*msg)) {
    return kernelClock.now().toMS() - before;
  } else {
    return SYS_ARCH_TIMEOUT;
  }
}

extern "C" int sys_mbox_valid(sys_mbox_t *mbox) {
  return *mbox != nullptr;
}

extern "C" void sys_mbox_set_invalid(sys_mbox_t *mbox) {
  *mbox = nullptr;
}

extern "C" sys_thread_t sys_thread_new(const char *name, lwip_thread_fn thread, void *arg, int stacksize, int prio) {
  Thread* t = Thread::create(stacksize + defaultStackSize);
  //  would need to define various THREAD_PRIO settings in opt.h
  // t->setPriority(prio);
  t->start((ptr_t)thread, arg);
  return t;
}

extern "C" sys_prot_t sys_arch_protect(void) {
  return lwipLock->acquire() - 1;
}

extern "C" void sys_arch_unprotect(sys_prot_t pval) {
  lwipLock->release();
}

extern "C" u32_t sys_now(void) {
 return kernelClock.now().toMS();
} 

extern "C" u32_t sys_jiffies(void) {
 KABORT();
 return 0;
}

extern "C" void lwip_assert(const char* const loc, int line, const char* const func, const char* const msg) {
  kassertprints(loc, line, func);
  kassertprinte(msg);
}

extern "C" void lwip_printf(const char* fmt, ...) {
  va_list args;
  va_start(args, fmt);
  ExternDebugPrintf(DBG::Lwip, fmt, args);
  va_end(args);
}

extern "C" int lwip_random() {
  return LocalProcessor::self()->random();
}
 