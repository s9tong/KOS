/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _Process_h_
#define _Process_h_ 1

#include "runtime/SynchronizedArray.h"
#include "kernel/AddressSpace.h"
#include "kernel/KernelHeap.h"
#include "kernel/Thread.h"
#include "world/Access.h"
#include "ulib/syscalls.h"

class Process : public AddressSpace {

  struct UserThread : public Thread {
    mword  index;              // TCB index, set during startup
    vaddr  ustackBottom;       // bottom of user stack, set up after invocation
    size_t ustackSize;         // size of user stack, set up after invocation

    volatile mword sigPending; // signals pending? only SIGTERM for now...

    CPU::UserMachContext uctx;      // fs register
    FloatingPointContext fpctx;     // FP context
    bool isrMode;

    UserThread(vaddr ksb, size_t kss, Process& p)
    : Thread(ksb, kss, p), sigPending(0), isrMode(false) {}

    static inline UserThread* create(vaddr mem, size_t kss, Process& p) {
      vaddr This = mem + kss - sizeof(UserThread);
      return new (ptr_t(This)) UserThread(mem, kss, p);
    }
  };

  static UserThread* CurrUT() {
    UserThread* ut = reinterpret_cast<UserThread*>(CurrStack());
    KASSERT(ut);
    return ut;
  }

  typedef Joinable<UserThread,ptr_t,KernelLock> TCB;
  typedef ManagedArray<TCB,TCB::Invalid,KernelAllocator> TCBStore;

  KernelLock tcbLock;
  TCBStore   tcbStore;
  size_t     threadCount;
  mword      index;

  enum State { Init, Running, Exiting } state;

  vaddr              sigTrampoline; // signal handler trampoline in user space / libc
  volatile mword     sigPending;    // signals pending?

  static void invokeUser(funcvoid2_t func, ptr_t arg1, ptr_t arg2) __noreturn;
  static void loadAndRun(Process*, char* fileName, uintptr_t len);
  inline funcvoid2_t load(char* fileName, uintptr_t len);

  inline mword createThreadInternal(ptr_t invoke, ptr_t wrapper, ptr_t func, ptr_t data);

public:
  SynchronizedArray<Access*,KernelAllocator,KernelLock> ioHandles;  // used in syscalls.cc
  ManagedArray<FifoSemaphore<KernelLock>*,1,KernelAllocator> semStore;   // used in syscalls.cc
  KernelLock                                                 semLock;    // used in syscalls.cc

  typedef Joinable<Process,int,KernelLock> PCB;
  typedef ManagedArray<PCB,PCB::Invalid,KernelAllocator> PCBStore;

  static PCBStore*  pcbStore; // used in syscalls.cc
  static KernelLock pcbLock;  // used in syscalls.cc

  Process() : tcbStore(1), threadCount(0), index(0), state(Init),
    sigTrampoline(0), sigPending(0), ioHandles(4) {
    ioHandles.store(knew<InputAccess>());
    ioHandles.store(knew<OutputAccess>(StdOut));
    ioHandles.store(knew<OutputAccess>(StdErr));
    ioHandles.store(knew<OutputAccess>(StdDbg));
  }
  virtual ~Process();

  mword getID() { return index; }
  static mword getCurrentThreadID() { return CurrUT()->index; }

  mword exec(const std::string& fn);
  mword startThread(funcvoid2_t wrapper, funcvoid1_t func, ptr_t data);
  void  detachThread(mword tid);
  void  exitThread(ptr_t result) __noreturn;
  int   joinThread(mword tid, ptr_t& result);
  int   signalThread(mword tid, mword sig);

  void detach();
  void exit(int result);
  static int join(mword pid, int& result);
  static int kill(mword pid, int sig);

  void setSignalTrampoline(vaddr st) { sigTrampoline = st; }

  void setSignal(mword sig) {
    __atomic_or_fetch(&sigPending, sig, __ATOMIC_RELAXED);
  }

  mword checkSignals() {
    if (state != Running) return 0; // user-level BSS not cleared
    if (CurrUT()->sigPending & SIGTERM) exitThread(nullptr);
    return __atomic_exchange_n(&sigPending, 0, __ATOMIC_RELAXED);
  }

  mword checkSignals(mword& tpl) {
    if (state != Running) return 0; // user-level BSS not cleared
    if (CurrUT()->sigPending & SIGTERM) exitThread(nullptr);
    if (!sigTrampoline) return 0;
    tpl = sigTrampoline;
    return __atomic_exchange_n(&sigPending, 0, __ATOMIC_RELAXED);
  }

  static void isrEntry()  { CurrUT()->isrMode = true; }
  static void isrExit()   { CurrUT()->isrMode = false; }
  static void restoreFP() { CurrUT()->fpctx.restore(); }

  virtual void preStackSwitch(Thread* currThread);
  virtual void postStackSwitch(Thread* newThread);
  virtual bool threadTerminated();
};

static inline Process& CurrProcess() {
  AddressSpace& as = CurrThread()->getAS();
  KASSERTN(&as != &defaultAS, ' ', &as, ' ', &defaultAS);
  return reinterpret_cast<Process&>(as);
}

#endif /* _Process_h_ */
