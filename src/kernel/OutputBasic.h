/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _OutputBasic_h_
#define _OutputBasic_h_ 1

// defined in Machine.cc
void Reboot(vaddr = 0) __noreturn;
void RebootDirect() __noreturn;

// defined in Output.cc
extern void kassertprints(const char* const loc, int line, const char* const func);
extern void kassertprinte(const char* const msg);
extern void kassertprinte(const unsigned long long num);
extern void kassertprinte(const FmtHex& ptr);
extern void kassertprinte();

#define KABORT()           {                      { kassertprints(  "KABORT: "       " in " __FILE__ ":", __LINE__, __func__); kassertprinte();    Reboot(); } }
#define KABORT1(msg)       {                      { kassertprints(  "KABORT: "       " in " __FILE__ ":", __LINE__, __func__); kassertprinte(msg); Reboot(); } }

#if TESTING_ENABLE_ASSERTIONS
#define KCHECK(expr)       { if slowpath(!(expr)) { kassertprints(  "KCHECK: " #expr " in " __FILE__ ":", __LINE__, __func__); kassertprinte();    } }
#define KCHECK1(expr,msg)  { if slowpath(!(expr)) { kassertprints(  "KCHECK: " #expr " in " __FILE__ ":", __LINE__, __func__); kassertprinte(msg); } }
#define KASSERT(expr)      { if slowpath(!(expr)) { kassertprints( "KASSERT: " #expr " in " __FILE__ ":", __LINE__, __func__); kassertprinte();    Reboot(); } }
#define KASSERT1(expr,msg) { if slowpath(!(expr)) { kassertprints( "KASSERT: " #expr " in " __FILE__ ":", __LINE__, __func__); kassertprinte(msg); Reboot(); } }
#else
#define KCHECK(expr)
#define KCHECK1(expr,msg)
#define KASSERT(expr)
#define KASSERT1(expr,msg)
#endif

#endif /* _OutputBasic_h_ */
