/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "runtime/RuntimeImpl.h"
#include "world/Access.h"
#include "machine/HardwareProcessor.h"

#include "ulib/syscalls.h"
#include "ulib/pthread.h"

/******* libc functions *******/

// for C-style 'assert' (e.g., from malloc.c)
extern "C" void __assert_func( const char* const file, size_t line,
  const char* const func, const char* const expr ) {
  KERR::outl("ASSERT: ", file, ':', line, ' ', func, ' ', expr);
  Reboot();
}

extern "C" void abort() {
  KABORT();
}

extern "C" void free(void* ptr) { KernelHeap::legacy_free(ptr); }
extern "C" void _free_r(_reent* r, void* ptr) { free(ptr); }
extern "C" void* malloc(size_t size) { return KernelHeap::legacy_malloc(size); }
extern "C" void* _malloc_r(_reent* r, size_t size) { return malloc(size); }

extern "C" void* calloc(size_t nmemb, size_t size) {
  void* ptr = malloc(nmemb * size);
  memset(ptr, 0, nmemb * size);
  return ptr;
}

extern "C" void* _calloc_r(_reent* r, size_t nmemb, size_t size) {
  return calloc(nmemb, size);
}

extern "C" void* realloc(void* ptr, size_t size) {
  KABORTN("realloc");
  return nullptr;
}

extern "C" void* _realloc_r(_reent* r, void* ptr, size_t size) {
  return realloc(ptr, size);
}

/******* syscall functions *******/

// libc exit calls atexit routines, then invokes _exit
extern "C" void _exit(int result) {
  CurrProcess().exit(result);
  LocalProcessor::lock(true);
  CurrProcess().exitThread(nullptr);
}

extern "C" int open(const char *path, int oflag, ...) {
  Process& p = CurrProcess();
  auto it = kernelFS.find(path);
  if (it == kernelFS.end()) return -ENOENT;
  return p.ioHandles.store(knew<FileAccess>(it->second));
}

extern "C" int close(int fildes) {
  Process& p = CurrProcess();
  Access* access = p.ioHandles.removeWait(fildes);
  if (!access) return -EBADF;
  delete access;
  return 0;
}

extern "C" ssize_t read(int fildes, void* buf, size_t nbyte) {
  // TODO: validate buf/nbyte
  Process& p = CurrProcess();
  Access* access = p.ioHandles.startAccess(fildes);
  if (!access) return -EBADF;
  ssize_t ret = access->read(buf, nbyte);
  p.ioHandles.stopAccess(fildes);
  return ret;
}

extern "C" ssize_t write(int fildes, const void* buf, size_t nbyte) {
  // TODO: validate buf/nbyte
  Process& p = CurrProcess();
  Access* access = p.ioHandles.startAccess(fildes);
  if (!access) return -EBADF;
  ssize_t ret = access->write(buf, nbyte);
  p.ioHandles.stopAccess(fildes);
  return ret;
}

extern "C" off_t lseek(int fildes, off_t offset, int whence) {
  Process& p = CurrProcess();
  Access* access = p.ioHandles.startAccess(fildes);
  if (!access) return -EBADF;
  ssize_t ret = access->lseek(offset, whence);
  p.ioHandles.stopAccess(fildes);
  return ret;
}

extern "C" int kill(pid_t pid, int sig) {
  return Process::kill(pid, sig);
}

extern "C" int waitpid(pid_t pid, int* wstatus, int options) {
  return Process::join(pid, *wstatus);
}

extern "C" pid_t getpid() {
  return CurrProcess().getID();
}

extern "C" pid_t getcid() {
  return LocalProcessor::getIndex();
}

extern "C" int usleep(useconds_t usecs) {
  CurrTimerQueue().sleep(Time::fromUS(usecs));
  return 0;
}

extern "C" int _mmap(void** addr, size_t len, int protflags, int fildes, off_t off) {
  // TODO: validate addr
#if TESTING_ENABLE_ASSERTIONS
  int prot = protflags & 0xf;
  int flags = protflags >> 4;
  KASSERTN(prot == 0, prot);
  KASSERTN(flags == 0, flags);
#endif
  KASSERTN(fildes == -1, fildes);
  KASSERTN(off == 0, off);
  vaddr va = CurrProcess().mmap<smallpl>(vaddr(*addr), len);
  if (va == topaddr) return -ENOMEM; // shouldn't happen currently...
  *addr = (void*)va;
  return 0;
}

extern "C" int _munmap(void* addr, size_t len) {
  CurrProcess().munmap<smallpl>(vaddr(addr), len);
  return 0;
}

extern "C" pthread_t _pthread_create(funcvoid2_t invoke, funcvoid1_t func, void* data) {
  return CurrProcess().startThread(invoke, func, data);
}

extern "C" int pthread_detach(pthread_t tid) {
  CurrProcess().detachThread(tid);
  return 0;
}

extern "C" void pthread_exit(void* result) {
  LocalProcessor::lock(true);
  CurrProcess().exitThread(result);
}

extern "C" int pthread_join(pthread_t tid, void** result) {
  // TODO: validate result
  return CurrProcess().joinThread(tid, *result);
}

extern "C" int pthread_kill(pthread_t tid, int signal) {
  return CurrProcess().signalThread(tid, signal);
}

extern "C" pthread_t pthread_self() {
  return Process::getCurrentThreadID();
}

extern "C" int pthread_yield() {
  Thread::yield();
  return 0;
}

extern "C" int semCreate(mword* rsid, mword init) {
  // TODO: validate rsid
  Process& p = CurrProcess();
  ScopedLock<KernelLock> sl(p.semLock);
  FifoSemaphore<KernelLock>* s = knew<FifoSemaphore<KernelLock>>(init);
  *rsid = p.semStore.put(s);
  return 0;
}

extern "C" int semDestroy(mword sid) {
  Process& p = CurrProcess();
  ScopedLock<KernelLock> sl(p.semLock);
  if (!p.semStore.valid(sid) || !p.semStore.access(sid)->empty()) return -1;
  kdelete(p.semStore.access(sid));
  p.semStore.remove(sid);
  return 0;
}

extern "C" int semP(mword sid) {
  Process& p = CurrProcess();
  p.semLock.acquire();
  if (p.semStore.valid(sid)) {
    p.semStore.access(sid)->P_unlock(p.semLock);
    return 0;
  } else {
    p.semLock.release();
    return -1;
  }
}

extern "C" int semV(mword sid) {
  Process& p = CurrProcess();
  ScopedLock<KernelLock> sl(p.semLock);
  if (!p.semStore.valid(sid)) return -1;
  p.semStore.access(sid)->V();
  return 0;
}

typedef int (*funcint4_t)(mword, mword, mword, mword);
extern "C" int privilege(ptr_t func, mword a1, mword a2, mword a3, mword a4) {
  return ((funcint4_t)func)(a1, a2, a3, a4);
}

extern "C" void _init_sigtrampoline(vaddr sigtramp) {
  CurrProcess().setSignalTrampoline(sigtramp);
}

/******* dummy functions *******/

extern "C" int fstat(int fildes, struct stat *buf) {
  KABORTN("fstat"); return -ENOSYS;
}

//extern "C" char *getenv(const char *name) {
//  DBG::outl(DBG::Libc, "LIBC/getenv: ", name);
//  return nullptr;
//}

extern "C" int isatty(int fd) {
  KABORTN("isatty"); return -ENOSYS;
}

//extern "C" void* sbrk(intptr_t increment) {
//  KABORTN("sbrk"); return (void*)-1;
//}

void* __dso_handle = nullptr;

typedef ssize_t (*syscall_t)(mword a1, mword a2, mword a3, mword a4, mword a5);
static const syscall_t syscalls[] = {
  syscall_t(_exit),
  syscall_t(open),
  syscall_t(close),
  syscall_t(read),
  syscall_t(write),
  syscall_t(lseek),
  syscall_t(kill),
  syscall_t(waitpid),
  syscall_t(getpid),
  syscall_t(getcid),
  syscall_t(usleep),
  syscall_t(_mmap),
  syscall_t(_munmap),
  syscall_t(_pthread_create),
  syscall_t(pthread_detach),
  syscall_t(pthread_exit),
  syscall_t(pthread_join),
  syscall_t(pthread_kill),
  syscall_t(pthread_self),
  syscall_t(pthread_yield),
  syscall_t(semCreate),
  syscall_t(semDestroy),
  syscall_t(semP),
  syscall_t(semV),
  syscall_t(privilege),
  syscall_t(_init_sigtrampoline)
};

static_assert(sizeof(syscalls)/sizeof(syscall_t) == SyscallNum::max, "syscall list error");

extern "C" ssize_t syscall_handler(mword x, mword a1, mword a2, mword a3, mword a4, mword a5) {
  if fastpath(x < SyscallNum::max) return syscalls[x](a1, a2, a3, a4, a5);
  DBG::outl(DBG::Tests, "unknown syscall: ", x);
  return -ENOSYS;
}

extern "C" mword syscall_signals() {
  return CurrProcess().checkSignals();
}
