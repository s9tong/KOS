/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _KernelLocks_h_
#define _KernelLocks_h_ 1

#include "generic/SpinLocks.h"
#include "generic/ScopedLocks.h"
#include "machine/HardwareProcessor.h"

template<typename SpinLock>
class KernelSpinLock : protected SpinLock {
public:
  bool test() const { return SpinLock::test(); }
  bool tryAcquire() {
    LocalProcessor::lock();
    if (SpinLock::tryAcquire()) return true;
    LocalProcessor::unlock();
    return false;
  }
  void acquire() {
    LocalProcessor::lock();
    SpinLock::acquire();
  }
  void release() {
    SpinLock::release();
    LocalProcessor::unlock();
  }
  void releaseHalt() {
    SpinLock::release();
    LocalProcessor::unlock<true>(true);
  }
};

template<>
class KernelSpinLock<BinaryOwnerLock<>> : protected BinaryOwnerLock<> {
public:
  mword tryAcquire() {
    LocalProcessor::lock();
    mword ret = BinaryOwnerLock::tryAcquire(LocalProcessor::getIndex());
    if (ret) return ret;
    LocalProcessor::unlock();
    return 0;
  }
  mword acquire(KernelSpinLock* l = nullptr) {
    LocalProcessor::lock();                  // LocalProcessor counts as well
    mword ret = BinaryOwnerLock::acquire(LocalProcessor::getIndex());
    if (l) l->release();
    return ret;
  }
  template<bool full = false>
  mword release() {
    mword ret = BinaryOwnerLock::release<full>(LocalProcessor::getIndex());
    LocalProcessor::unlock();                // LocalProcessor counts as well
    return ret;
  }
  template<bool full = false>
  mword releaseHalt() {
    mword ret = BinaryOwnerLock::release<full>(LocalProcessor::getIndex());
    LocalProcessor::unlock<true>(true);
    return ret;
  }
};

template<>
class KernelSpinLock<MCSLock> : protected MCSLock {
public:
  typedef MCSLock::Node Node;
  void acquire(Node& n) {
    LocalProcessor::lock();
    MCSLock::acquire(n);
  }
  void release(Node& n) {
    MCSLock::release(n);
    LocalProcessor::unlock();
  }
  void releaseHalt(Node& n) {
    MCSLock::release(n);
    LocalProcessor::unlock<true>(true);
  }
};

class KernelLockRW : public SpinLockRW {
public:
  bool tryAcquireRead() {
    LocalProcessor::lock();
    if (SpinLockRW::tryAcquireRead()) return true;
    LocalProcessor::unlock();
    return false;
  }
  void acquireRead() {
    LocalProcessor::lock();
    SpinLockRW::acquireRead();
  }
  bool tryAcquireWrite() {
    LocalProcessor::lock();
    if (SpinLockRW::tryAcquireWrite()) return true;
    LocalProcessor::unlock();
    return false;
  }
  void acquireWrite() {
    LocalProcessor::lock();
    SpinLockRW::acquireWrite();
  }
  void release() {
    SpinLockRW::release();
    LocalProcessor::unlock();
  }
};

typedef KernelSpinLock<BinaryLock<>>      KernelLock;
typedef KernelSpinLock<TicketLock>        KernelTicketLock;
typedef KernelSpinLock<BinaryOwnerLock<>> KernelOwnerLock;
typedef KernelSpinLock<MCSLock>           KernelMCSLock;

template <>
class ScopedLock<KernelMCSLock> {
  KernelMCSLock& lk;
  KernelMCSLock::Node ln;
public:
  ScopedLock(KernelMCSLock& lk) : lk(lk) { lk.acquire(ln); }
  ~ScopedLock() { lk.release(ln); }
};

template <>
class ScopedLock<LocalProcessor> {
public:
  ScopedLock() { LocalProcessor::lock(); }
  ~ScopedLock() { LocalProcessor::unlock(); }
};

#endif /* _SpinLock_h_ */
