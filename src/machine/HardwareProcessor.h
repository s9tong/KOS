/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _HardwareProcessor_h_
#define _HardwareProcessor_h_ 1

#include "machine/asmdecl.h"
#include "machine/asmshare.h"
#include "machine/APIC.h"
#include "machine/CPU.h"
#include "machine/Descriptors.h"
#include "machine/Memory.h" // apicAddr, ioApicAddr

class StackContext;
class FrameManager;
class KernelProcessor;
struct Runtime;

static APIC*   MappedAPIC()   { return   (APIC*)apicAddr; }
static IOAPIC* MappedIOAPIC() { return (IOAPIC*)ioApicAddr; }

class HardwareProcessor {
  friend class LocalProcessor;      // member offsets for %gs-based access
  friend class Machine;             // init and setup routines

  mword            index;
  mword            apicID;
  mword            systemID;
  mword            lockCount;
  StackContext*    currStack;
  KernelProcessor* currProc;
  FrameManager*    currFM;
  TaskStateSegment tss;

  /* task state segment: kernel stack for interrupts/exceptions */
  static const unsigned int nmiIST = 1;
  static const unsigned int dbfIST = 2;
  static const unsigned int stfIST = 3;

  // layout for syscall/sysret, because of (strange) rules for SYSCALL_LSTAR
  // SYSCALL_LSTAR essentially forces userCS = userDS + 1
  // layout for sysenter/sysexit would be: kernCS, kernDS, userCS, userDS
  // also: kernDS = 2 consistent with convention and segment setup in boot.S
  static const unsigned int kernCS  = 1;
  static const unsigned int kernDS  = 2;
  static const unsigned int userDS  = 3;
  static const unsigned int userCS  = 4;
  static const unsigned int tssSel  = 5; // TSS entry uses 2 entries
  static const unsigned int maxGDT  = 7;
  SegmentDescriptor gdt[maxGDT];

  void setupGDT(uint32_t n, uint32_t dpl, bool code)    __section(".boot.text");
  void setupTSS(uint32_t num, paddr addr)               __section(".boot.text");

  HardwareProcessor(const HardwareProcessor&) = delete;            // no copy
  HardwareProcessor& operator=(const HardwareProcessor&) = delete; // no assignment

  void install() {
    MSR::write(MSR::GS_BASE, mword(this)); // store 'this' in gs
    MSR::write(MSR::KERNEL_GS_BASE, 0);    // later: store user value in shadow gs
  }

  void setup(mword i, mword a, mword s, FrameManager& fm) {
    index = i;
    apicID = a;
    systemID = s;
    currFM = &fm;
  }

  void init(paddr, InterruptDescriptor*, size_t, bool)  __section(".boot.text");

protected:
  void setupCurrStack(StackContext* s) { currStack = s; }

public:
  HardwareProcessor(KernelProcessor* s) : index(0), apicID(0), systemID(0),
    lockCount(1), currStack(nullptr), currProc(s), currFM(nullptr) {}
  mword getIndex() const { return index; }
  void sendIPI(uint8_t vec) { MappedAPIC()->sendIPI(apicID, vec); }
  void sendWakeIPI() { sendIPI(APIC::WakeIPI); }
  void sendPreemptIPI() { sendIPI(APIC::PreemptIPI); }
} __packed __caligned;

class LocalProcessor {
  template<bool halt=false>
  static void enableInterrupts() {
    asm volatile("sti" ::: "memory");
    if (halt) asm volatile("hlt" ::: "memory"); // sti;hlt atomic on Intel/AMD
  }
  static void disableInterrupts() {
    asm volatile("cli" ::: "memory");
  }
  static void incLockCount() {
    asm volatile("addq $1, %%gs:%c0" :: "i"(offsetof(HardwareProcessor, lockCount)) : "cc", "memory");
  }
  static void decLockCount() {
    asm volatile("subq $1, %%gs:%c0" :: "i"(offsetof(HardwareProcessor, lockCount)) : "cc", "memory");
  }

  template<typename T, mword offset> static T get() {
    T x;
    asm volatile("movq %%gs:%c1, %0" : "=r"(x) : "i"(offset));
    return x;
  }

  template<typename T, mword offset> static void set(T x) {
    asm volatile("movq %0, %%gs:%c1" :: "r"(x), "i"(offset) : "memory");
  }

public:
  static void configInterrupts(bool irqs) {
    MappedAPIC()->setFlatMode();         // set flat logical destination mode
    MappedAPIC()->setLogicalDest(irqs ? 0x01 : 0x00); // join irq group
    MappedAPIC()->setTaskPriority(0x00); // accept all interrupts
    MappedAPIC()->enable(0xff);          // enable APIC, set spurious IRQ to 0xff
  }

  static mword getIndex() {
    return get<mword, offsetof(HardwareProcessor, index)>();
  }
  static mword getApicID() {
    return get<mword, offsetof(HardwareProcessor, apicID)>();
  }
  static mword getSystemID() {
    return get<mword, offsetof(HardwareProcessor, systemID)>();
  }

  static mword getLockCount() {
    return get<mword, offsetof(HardwareProcessor, lockCount)>();
  }
  static mword checkLock() {
    KASSERT1(CPU::interruptsDisabled() == bool(getLockCount()), getLockCount());
    return getLockCount();
  }
  static void lockFake() {
    KASSERT(CPU::interruptsDisabled());
    incLockCount();
  }
  static void unlockFake() {
    KASSERT(CPU::interruptsDisabled());
    decLockCount();
  }
  static void lock(bool check = false) {
    KASSERT1(!check || (checkLock() == 0), getLockCount());
    // despite looking like trouble, I believe this is safe: race could cause
    // multiple calls to disableInterrupts(), but this is no problem!
    if (getLockCount() == 0) disableInterrupts();
    incLockCount();
  }
  template<bool halt=false>
  static void unlock(bool check = false) {
    KASSERT1(!check || (checkLock() == 1), getLockCount());
    decLockCount();
    // no races here (interrupts disabled)
    if (getLockCount() == 0) enableInterrupts<halt>();
  }

  static StackContext* getCurrStack() {
    return get<StackContext*, offsetof(HardwareProcessor, currStack)>();
  }
  static void setCurrStack(StackContext* x, _friend<Runtime>) {
    set<StackContext*, offsetof(HardwareProcessor, currStack)>(x);
  }
  static FrameManager* getCurrFM() {
    return get<FrameManager*, offsetof(HardwareProcessor, currFM)>();
  }

  static KernelProcessor* self() {
    return get<KernelProcessor*, offsetof(HardwareProcessor, currProc)>();
  }
  static void setKernelStack(StackContext* currStack) {
    const mword o = offsetof(HardwareProcessor, tss) + offsetof(TaskStateSegment, rsp);
    static_assert(o == TSSRSP, "TSSRSP");
    set<StackContext*, o>(currStack);     // StackContext* = top of stack
  }
};

static inline StackContext* CurrStack() {
  StackContext* s = LocalProcessor::getCurrStack();
  GENASSERT(s);
  return s;
}

static FrameManager& CurrFM() {
  FrameManager* fm = LocalProcessor::getCurrFM();
  KASSERT(fm);
  return *fm;
}

#endif /* HardwareProcessor_h_ */
