/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _BlockingSync_h_
#define _BlockingSync_h_ 1

#include "generic/Benaphore.h"
#include "runtime/StackContext.h"

#include <list>
#include <map>
#include <set>

// RULE: can acquire BlockingQueue lock after Timer lock, but not vice versa!
template<typename Lock>
class TimerQueueGeneric {
  friend class TimeoutInfo;
  template<typename> friend class TimeoutSynchronizationInfo;
  Lock lock;
  std::multimap<Time,StackContext*> queue;
  TimerStats* stats;
public:
  TimerQueueGeneric() { stats = new TimerStats(this); }
  typedef typename std::multimap<Time,StackContext*>::iterator iterator;
  iterator insertRelative(StackContext& sc, const Time& timeout) {
    lock.acquire();
    Time now = Runtime::now();
    Runtime::debugB( "Stack ", FmtHex(&sc), " set timeout ", timeout + now);
    iterator ret = queue.insert( {timeout + now, &sc} ); // set up timeout
    if (ret == queue.begin()) Runtime::notifyTimeout(timeout);
    return ret;
  }
  iterator insertAbsolute(StackContext& sc, const Time& timeout, const Time& now) {
    lock.acquire();
    Runtime::debugB( "Stack ", FmtHex(&sc), " set timeout ", timeout);
    iterator ret = queue.insert( {timeout, &sc} ); // set up timeout
    if (ret == queue.begin()) Runtime::notifyTimeout(timeout - now);
    return ret;
  }
  inline void sleep(const Time& timeout);
  inline bool checkExpiry(const Time& now, Time& newTime);
};

typedef SystemLock TimerQueueLock;
typedef TimerQueueGeneric<TimerQueueLock> TimerQueue;
inline TimerQueue& CurrTimerQueue();

class ResumeInfo {
  void unlock() {}
  template<typename Lock, typename... Args>
  void unlock(Lock& bl, Args&... ol) {
    bl.release();
    unlock(ol...);
  }
protected:
  template<typename... Args>
  void unlockAndSuspend(StackContext& cs, bool resumeRace, Args&... ol) {
    cs.prepareSuspend(_friend<ResumeInfo>());
    if (resumeRace) cs.setupResumeRace(*this, _friend<ResumeInfo>());
    Runtime::DisablePreemption(); // unlock would otherwise enable preemption
    unlock(ol...);
    cs.suspend(_friend<ResumeInfo>());
    Runtime::EnablePreemption();  // enable preemption now
  }
public:
  virtual void cancelTimeout() {}
  virtual void cancelSync(StackContext* ot = nullptr) {}
};

class TimeoutInfo : public virtual ResumeInfo {
protected:
  TimerQueue& tQueue;
  TimerQueue::iterator titer;
  void prepareRelative(StackContext& cs, const Time& timeout) {
    titer = tQueue.insertRelative(cs, timeout);
  }
  void prepareAbsolute(StackContext& cs, const Time& timeout, const Time& now) {
    titer = tQueue.insertAbsolute(cs, timeout, now);
  }
public:
  TimeoutInfo(TimerQueue& tq) : tQueue(tq) {}
  void suspendRelative(const Time& timeout, StackContext& cs = *CurrStack()) {
    prepareRelative(cs, timeout);
    unlockAndSuspend(cs, true, tQueue.lock);
  }
  virtual void cancelTimeout() {
    ScopedLock<TimerQueueLock> al(tQueue.lock);
    tQueue.queue.erase(titer);
  }
};

template<typename Lock, bool withQueue>
class SynchronizationInfo : public virtual ResumeInfo {
protected:
  Lock& lock;
  bool timedOut;
  void prepare() {
    static_assert(withQueue == false, "wrong 'suspend' without queue");
    GENASSERT(lock.test());
  }
  void prepare(StackContext& cs, StackList<>& queue) {
    static_assert(withQueue == true, "wrong 'suspend' with queue");
    GENASSERT(lock.test());
    queue.push_back(cs);
  }
public:
  SynchronizationInfo(Lock& l) : lock(l), timedOut(false) {}
  bool suspend(bool resumeRace = true, StackContext& cs = *CurrStack()) {
    prepare();
    unlockAndSuspend(cs, resumeRace, lock);
    return !timedOut;
  }
  bool suspend(StackList<>& queue, StackContext& cs = *CurrStack()) {
    prepare(cs, queue);
    unlockAndSuspend(cs, true, lock);
    return !timedOut;
  }
  virtual void cancelSync(StackContext* ot) {
    timedOut = true;
    if (!ot) return;
    ScopedLock<Lock> al(lock);
    StackList<>::remove(*ot);
  }
};

template<typename Lock>
class TimeoutSynchronizationInfo : public TimeoutInfo, public SynchronizationInfo<Lock,true> {
  using BaseSI = SynchronizationInfo<Lock,true>;
  using BaseSI::lock;
  using BaseSI::timedOut;
public:
  TimeoutSynchronizationInfo(TimerQueue& tq, Lock& l) : TimeoutInfo(tq), BaseSI(l) {}
  bool suspendAbsolute(StackList<>& queue, const Time& timeout, const Time& now, StackContext& cs = *CurrStack()) {
    BaseSI::prepare(cs, queue);
    TimeoutInfo::prepareAbsolute(cs, timeout, now);
    unlockAndSuspend(cs, true, lock, tQueue.lock);
    return !timedOut;
  }
};

template<typename Lock>
inline void TimerQueueGeneric<Lock>::sleep(const Time& timeout) {
  TimeoutInfo ti(*this);
  Runtime::debugB( "Stack ", FmtHex(CurrStack()), " sleep ", timeout);
  ti.suspendRelative(timeout);
}

template<typename Lock>
inline bool TimerQueueGeneric<Lock>::checkExpiry(const Time& now, Time& newTime) {
  bool retcode = false;
  std::list<std::pair<StackContext*,ResumeInfo*>> fireList; // defer event locks
  lock.acquire();
  int cnt = 0;
  for (auto it = queue.begin(); it != queue.end(); ) {
    if (it->first > now) {
      retcode = true;
      newTime = it->first - now;
  break;
    }
#if TESTING_ENABLE_STATISTICS
    cnt += 1;
#endif
    StackContext* t = it->second;
    ResumeInfo* ri = t->raceResume();
    if (ri) {
      it = queue.erase(it);
      fireList.push_back( {t, ri} );
    } else {
      it = next(it);
    }
  }
  stats->events.add(cnt);
  lock.release();
  for (auto f : fireList) {                          // timeout lock released
    f.second->cancelSync(f.first);                   // can acquire event locks
    f.first->resume<true>();
    Runtime::debugB( "Stack ", FmtHex(f.first), " timed out");
  }
  return retcode;
}

template<typename Lock>
class BlockingQueue {
  StackList<> queue;

  BlockingQueue(const BlockingQueue&) = delete;            // no copy
  BlockingQueue& operator=(const BlockingQueue&) = delete; // no assignment

public:
  BlockingQueue() = default;
  ~BlockingQueue() { GENASSERT(empty()); }
  bool empty() const { return queue.empty(); }

  void clear(Lock& lock) {
    GENASSERT(lock.test());
    StackContext* s = queue.front();
    while (s != queue.edge()) {
      ResumeInfo* ri = s->raceResume();
      StackContext* ns = StackList<>::next(*s);
      if (ri) {
        ri->cancelTimeout();
        ri->cancelSync();
        StackList<>::remove(*s);
      }
      s = ns;
    }
    lock.release();
    while (!empty()) Pause();     // wait for timed out events to disappear
  }

  // suspend releases lock; returns 'false' on cancel/timeout/nonblocking
  bool block(Lock& lock, bool wait) {
    if (wait) {
      SynchronizationInfo<Lock,true> si(lock);
      Runtime::debugB( "Stack ", FmtHex(CurrStack()), " blocking on ", FmtHex(&queue));
      return si.suspend(queue);
    }
    lock.release();
    return false;
  }

  bool block(Lock& lock, const Time& timeout, TimerQueue& tq = CurrTimerQueue()) {
    Time now = Runtime::now();
    if (timeout > now) {
      TimeoutSynchronizationInfo<Lock> tsi(tq, lock);
      Runtime::debugB( "Stack ", FmtHex(CurrStack()), " blocking on ", FmtHex(&queue), " timeout ", timeout);
      return tsi.suspendAbsolute(queue, timeout, now);
    }
    lock.release();
    return false;
  }

  StackContext* unblock(bool resumeNow = true) {       // not concurrency-safe; better hold lock
    for (StackContext* s = queue.front(); s != queue.edge(); s = StackList<>::next(*s)) {
      ResumeInfo* ri = s->raceResume();
      if (ri) {
        StackList<>::remove(*s);
        ri->cancelTimeout();
        if (resumeNow) s->resume<true>();
        Runtime::debugB( "Stack ", FmtHex(&s), " resumed from ", FmtHex(&queue));
        return s;
      }
    }
    return nullptr;
  }
};

template<typename Lock, bool Binary = false, typename BQ = BlockingQueue<Lock>>
class FifoSemaphore {
  Lock lock;
  ssize_t counter;
  BQ bq;

  template<typename... Args>
  bool internalP(const Args&... args) {
    if (counter < 1) return bq.block(lock, args...);
    counter -= 1;
    lock.release();
    return true;
  }

  StackContext* internalV(bool resumeNow = true) {
    StackContext* s = bq.unblock(resumeNow);
    if (!s) {
      if (Binary) counter = 1;
      else counter += 1;
    }
    return s;
  }

public:
  explicit FifoSemaphore(ssize_t c = 0) : counter(c) {}
  // baton passing requires serialization at destruction
  ~FifoSemaphore() { ScopedLock<Lock> sl(lock); }
  bool empty() { return bq.empty(); }
  bool open() { return counter >= 1; }
  ssize_t getValue() { return counter; }

  void reset(ssize_t c = 0) {
    lock.acquire();
    counter = c;
    bq.clear(lock);
  }

  bool P(bool wait = true)    { lock.acquire(); return internalP(wait); }
  bool P(const Time& timeout) { lock.acquire(); return internalP(timeout); }
  bool tryP() { return P(false); }

  template<typename Lock2>
  bool P_unlock(Lock2& l) {
    lock.acquire();
    l.release();
    return internalP(true);
  }

  void P_fake(size_t c = 1) {
    ScopedLock<Lock> al(lock);
    if (Binary) counter = 0;
    else counter -= c;
  }

  void V() {
    ScopedLock<Lock> al(lock);
    internalV();
  }

  void V_bulk(ProcessorResumeSet& procSet) {
    ScopedLock<Lock> al(lock);
    StackContext* s = internalV(false);
    if (s) {
      VirtualProcessor* proc = s->resume<false>();
      if (proc) procSet[proc].push(*s);
    }
  }
};

template<typename Lock, typename BQ = BlockingQueue<Lock>>
class BargingSemaphore {
  Lock lock;
  ssize_t counter;
  BQ bq;

  template<typename... Args>
  bool internalP(const Args&... args) {
    for (;;) {
      lock.acquire();
      if (counter >= 1) break;
      if (!bq.block(lock, args...)) return false;
    }
    counter -= 1;
    lock.release();
    return true;
  }

  StackContext* internalV(bool resumeNow = true) {
    ScopedLock<Lock> al(lock);
    counter += 1;
    return bq.unblock(resumeNow);
  }

public:
  explicit BargingSemaphore(ssize_t c = 0) : counter(0) {}
  bool empty() { return bq.empty(); }
  bool open() { return getValue() >= 1; }
  ssize_t getValue() { return counter; }

  void reset(ssize_t c = 0) {
    lock.acquire();
    counter = c;
    bq.clear(lock);
  }

  bool P(bool wait = true)    { return internalP(wait); }
  bool P(const Time& timeout) { return internalP(timeout); }
  bool tryP() { return P(false); }

  void V() {
    internalV();
  }

  void V_bulk(ProcessorResumeSet& procSet) {
    StackContext* s = internalV(false);
    if (s) {
      VirtualProcessor* proc = s->resume<false>();
      if (proc) procSet[proc].push(*s);
    }
  }
};

template<typename Lock, bool OwnerLock = false, typename BQ = BlockingQueue<Lock>>
class FifoMutex {
  Lock lock;
  StackContext* owner;
  BQ bq;

protected:
  template<typename... Args>
  bool internalAcquire(const Args&... args) {
    StackContext* cs = CurrStack();
    lock.acquire();
    if (!owner) {
      owner = cs;
    } else if (owner == cs) {
      GENASSERT1(OwnerLock, FmtHex(owner));
    } else {
      return bq.block(lock, args...);
    }
    lock.release();
    return true;
  }

public:
  FifoMutex() : owner(nullptr) {}
  // baton passing requires serialization at destruction
  ~FifoMutex() { ScopedLock<Lock> sl(lock); }
  bool test() const { return owner != nullptr; }

  bool acquire(bool wait = true)    { return internalAcquire(wait); }
  bool acquire(const Time& timeout) { return internalAcquire(timeout); }
  bool tryAcquire() { return acquire(false); }

  void release() {
    ScopedLock<Lock> al(lock);
    GENASSERT1(owner == CurrStack(), FmtHex(owner));
    owner = bq.unblock();
  }
};

template<typename Lock, bool OwnerLock = false, typename BQ = BlockingQueue<Lock>>
class BargingMutex {
  Lock lock;
  StackContext* owner;
  BQ bq;

protected:
  template<typename... Args>
  bool internalAcquire(const Args&... args) {
    StackContext* cs = CurrStack();
    for (;;) {
      lock.acquire();
      if (!owner) break;
      if (owner == cs) { GENASSERT1(OwnerLock, FmtHex(owner)); break; }
      if (!bq.block(lock, args...)) return false;
    }
    owner = cs;
    lock.release();
    return true;
  }

public:
  BargingMutex() : owner(nullptr) {}
  bool test() const { return owner != nullptr; }

  bool acquire(bool wait = true)    { return internalAcquire(wait); }
  bool acquire(const Time& timeout) { return internalAcquire(timeout); }
  bool tryAcquire() { return acquire(false); }

  void release() {
    ScopedLock<Lock> al(lock);
    GENASSERT1(owner == CurrStack(), FmtHex(owner));
    owner = nullptr;
    bq.unblock();
  }
};

template<typename Semaphore, bool OwnerLock, size_t SpinStart, size_t SpinEnd>
class SpinMutex {
  StackContext* owner;
  Semaphore sem;

protected:
  template<typename... Args>
  bool internalAcquire(const Args&... args) {
    StackContext* cs = CurrStack();
    StackContext* exp = nullptr;
    if (__atomic_compare_exchange_n(&owner, &exp, cs, false, __ATOMIC_SEQ_CST, __ATOMIC_RELAXED)) {
      return true;
    } else if (exp == cs) {
      GENASSERT1(OwnerLock, FmtHex(owner));
      return true;
    }
    size_t spin = SpinStart;
    for (;;) {
      if (spin <= SpinEnd) {
        for (size_t i = 0; i < spin; i += 1) Pause();
        spin += spin;
      } else {
        if (!sem.P(args...)) return false;
        spin = SpinStart;
      }
      exp = nullptr;
      if (__atomic_compare_exchange_n(&owner, &exp, cs, false, __ATOMIC_SEQ_CST, __ATOMIC_RELAXED)) return true;
    }
  }

public:
  SpinMutex() : owner(nullptr), sem(1) {}
  bool test() const { return owner != nullptr; }

  bool acquire(bool wait = true)    { return internalAcquire(wait); }
  bool acquire(const Time& timeout) { return internalAcquire(timeout); }
  bool tryAcquire() { return acquire(false); }

  void release() {
    GENASSERT1(owner == CurrStack(), FmtHex(owner));
    __atomic_store_n(&owner, nullptr, __ATOMIC_RELAXED); // memory sync via sem.V()
    sem.V();
  }
};

template<typename BaseMutex>
class OwnerMutex : private BaseMutex {
  size_t counter;

public:
  OwnerMutex() : counter(0) {}

  size_t acquire(bool wait = true) {
    if (BaseMutex::internalAcquire(wait)) return ++counter; else return 0;
  }

  size_t acquire(const Time& timeout) {
    if (BaseMutex::internalAcquire(timeout)) return ++counter; else return 0;
  }

  size_t tryAcquire() { return acquire(false); }

  size_t release() {
    if (--counter > 0) return counter;
    BaseMutex::release();
    return 0;
  }
};

template<typename Lock>
#if TESTING_MUTEX_FIFO
class Mutex : public FifoMutex<Lock> {};
#elif TESTING_MUTEX_BARGING
class Mutex : public BargingMutex<Lock> {};
#elif TESTING_MUTEX_SPIN
class Mutex : public SpinMutex<Benaphore<BargingSemaphore<Lock>>, false, 4, 1024> {};
#else
class Mutex : public SpinMutex<Benaphore<BargingSemaphore<Lock>>, false, 1, 0> {};
#endif

// simple blocking RW lock: release alternates; new readers block when writer waits -> no starvation
template<typename Lock, typename BQ = BlockingQueue<Lock>>
class LockRW {
  Lock lock;
  ssize_t state;                    // -1 writer, 0 open, >0 readers
  BQ bqR;
  BQ bqW;

  template<typename... Args>
  bool internalAR(const Args&... args) {
    lock.acquire();
    if (state < 0 || !bqW.empty()) {
      if (!bqR.block(lock, args...)) return false;
      lock.acquire();
      bqR.unblock();                // waiting readers can barge after writer
    }
    state += 1;
    lock.release();
    return true;
  }

  template<typename... Args>
  bool internalAW(const Args&... args) {
    lock.acquire();
    if (state != 0) {
      if (!bqW.block(lock, args...)) return false;
      lock.acquire();
    }
    state -= 1;
    lock.release();
    return true;
  }

public:
  LockRW() : state(0) {}

  bool acquireRead(bool wait = true)    { return internalAR(wait); }
  bool acquireRead(const Time& timeout) { return internalAR(timeout); }
  bool tryAcquireRead() { return acquireRead(false); }

  bool acquireWrite(bool wait = true)    { return internalAW(wait); }
  bool acquireWrite(const Time& timeout) { return internalAW(timeout); }
  bool tryAcquireWrite() { return acquireWrite(false); }

  void release() {
    ScopedLock<Lock> al(lock);
    GENASSERT(state != 0);
    if (state > 0) {             // reader leaves; if open -> writer next
      state -= 1;
      if (state > 0) return;
      if (!bqW.unblock()) bqR.unblock();
    } else {                     // writer leaves -> readers next
      GENASSERT(state == -1);
      state += 1;
      if (!bqR.unblock()) bqW.unblock();
    }
  }
};

// simple blocking barrier
template<typename Lock, typename BQ = BlockingQueue<Lock>>
class Barrier {
  Lock lock;
  size_t target;
  size_t counter;
  BQ bq;
public:
  Barrier(size_t t = 1) : target(t), counter(0) { GENASSERT(t); }
  void reset(size_t t = 1) {
    GENASSERT(t);
    lock.acquire();
    target = t;
    counter = 0;
    bq.clear(lock);
  }
  bool wait() {
    lock.acquire();
    counter += 1;
    if (counter == target) {
      while (bq.unblock());
      counter = 0;
      lock.release();
      return true;
    } else {
      bq.block(lock, true);
      return false;
    }
  }
};

// simple blocking condition variable: assume caller holds lock
template<typename Lock, typename BQ = BlockingQueue<Lock>>
class Condition {
  BQ bq;
public:
  bool empty() { return bq.empty(); }
  void reset(Lock& lock) { bq.clear(lock); }

  template<bool Reacquire = false>
  bool wait(Lock& lock) {
    bool ret = bq.block(lock, true);
    if (Reacquire) lock.acquire();
    return ret;
  }

  template<bool Reacquire = false>
  bool wait(Lock& lock, const Time& timeout) {
    bool ret = bq.block(lock, timeout);
    if (Reacquire) lock.acquire();
    return ret;
  }

  template<bool Broadcast = false>
  void signal() { while (bq.unblock() && Broadcast); }
};

// cf. condition variable: assume caller to wait/post holds lock
template<typename Lock>
class SynchronizedFlag {

public:
  enum State { Running = 0, Dummy = 1, Posted = 2, Detached = 4 };

protected:
  union {                             // 'waiter' set <-> 'state == Waiting'
    StackContext* waiter;
    State         state;
  };

public:
  static const State Invalid = Dummy; // dummy bit never set (for ManagedArray)
  SynchronizedFlag(State s = Running) : state(s) {}
  void reset()          { state = Running; }
  bool posted()   const { return state == Posted; }
  bool detached() const { return state == Detached; }

  bool wait(Lock& lock) {             // returns false, if detached
    if (state == Running) {
      SynchronizationInfo<Lock,false> si(lock);
      waiter = CurrStack();
      si.suspend(false, *waiter);
      lock.acquire();                 // reacquire lock to check state
    }
    if (state == Posted) return true;
    if (state == Detached) return false;
    GENABORT1(FmtHex(state));
  }

  bool post() {                       // returns false, if already detached
    GENASSERT(state != Posted);       // check for spurious posts
    if (state == Detached) return false;
    if (state != Running) waiter->resume();
    state = Posted;
    return true;
  }

  void detach() {                     // returns false, if already posted or detached
    GENASSERT(state != Detached && state != Posted);
    if (state != Running) waiter->resume();
    state = Detached;
  }
};

// cf. condition variable: assume caller to wait/post holds lock
template<typename Runner, typename Result, typename Lock>
class Joinable : public SynchronizedFlag<Lock> {
  using Baseclass = SynchronizedFlag<Lock>;
protected:
  union {
    Runner* runner;
    Result  result;
  };

public:
  Joinable(Runner* t) : runner(t) {}

  bool wait(Lock& bl, Result& r) {
    bool retcode = Baseclass::wait(bl);
    r = retcode ? result : 0; // result is available after returning from wait
    return retcode;
  }

  bool post(Result r) {
    bool retcode = Baseclass::post();
    if (retcode) result = r;  // still holding lock while setting result
    return retcode;
  }

  Runner* getRunner() const { return runner; }
};

template<typename Lock>
class SyncPoint : public SynchronizedFlag<Lock> {
  using Baseclass = SynchronizedFlag<Lock>;
  typedef typename Baseclass::State State;
  Lock lock;
public:
  SyncPoint(State s = Baseclass::Running) : Baseclass(s) {}
  void reset()  { ScopedLock<Lock> al(lock); Baseclass::reset(); }
  bool wait()   { ScopedLock<Lock> al(lock); return Baseclass::wait(lock); }
  bool post()   { ScopedLock<Lock> al(lock); return Baseclass::post(); }
  void detach() { ScopedLock<Lock> al(lock); Baseclass::detach(); }
};

#endif /* _BlockingSync_h_ */
