/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "runtime/RuntimeImpl.h"
#include "runtime/Cluster.h"
#include "runtime/StackContext.h"

StackContext::StackContext(VirtualProcessor& proc)
: resumeProcessor(&proc), priority(defPriority), balanceCounter(0),
  affinity(false), suspendState(Running), resumeInfo(nullptr) {
  resumeProcessor->addStack(_friend<StackContext>());
}

StackContext::StackContext(Cluster& cluster, bool bg)
: StackContext(cluster.placement(_friend<StackContext>(), bg)) {}

template<StackContext::SwitchCode Code>
inline bool StackContext::switchStack() {
  // various checks
  static_assert(Code == Idle || Code == Migrate || Code == Yield || Code == Preempt || Code == Suspend || Code == Terminate, "Illegal SwitchCode");
  CHECK_PREEMPTION(0);
  GENASSERTN(this == CurrStack(), FmtHex(this), ' ', FmtHex(CurrStack()));

  // pick next stack
  BaseProcessor& currProcessor = CurrProcessor();
  StackContext* nextStack = currProcessor.schedule(_friend<StackContext>());

  // continue current stack?
  if (Code == Idle || Code == Yield || Code == Preempt) {
    if (nextStack->priority == idlePriority) {
       Runtime::noStackSwitch(this);
       return false;
    }
  }

  // debug output and another check
  Runtime::debugS("Stack switch <", char(Code), "> on ", FmtHex(&currProcessor),": ", FmtHex(this), " (to ", FmtHex(resumeProcessor), ") -> ", FmtHex(nextStack));
  GENASSERTN(nextStack != this, FmtHex(this), ' ', FmtHex(nextStack));

  // context switch
  Runtime::preStackSwitch(this, nextStack);
  switch (Code) {
    case Idle:      stackSwitch(this, postIdle,      &stackPointer, nextStack->stackPointer); break;
    case Migrate:
    case Yield:     stackSwitch(this, postYield,     &stackPointer, nextStack->stackPointer); break;
    case Preempt:   stackSwitch(this, postPreempt,   &stackPointer, nextStack->stackPointer); break;
    case Suspend:   stackSwitch(this, postSuspend,   &stackPointer, nextStack->stackPointer); break;
    case Terminate: stackSwitch(this, postTerminate, &stackPointer, nextStack->stackPointer); break;
  }
  stackPointer = 0;                // mark stack in use for gdb
  Runtime::postStackSwitch(this);  // RT-specific functionality
  return true;
}

// idle -> do nothing
void StackContext::postIdle(StackContext* prevStack) {
  CHECK_PREEMPTION(0);
}

// yield -> resume right away
void StackContext::postYield(StackContext* prevStack) {
  CHECK_PREEMPTION(0);
  prevStack->resumeInternal();
}

// not sure about previous stack
void StackContext::postPreempt(StackContext* prevStack) {
  if fastpath(prevStack->priority < idlePriority) prevStack->resumeInternal();
}

// if resumption already triggered -> resume right away
void StackContext::postSuspend(StackContext* prevStack) {
  CHECK_PREEMPTION(0);
  SuspendState prevState = Prepared;
  bool suspended = __atomic_compare_exchange_n( &prevStack->suspendState, &prevState, Suspended, false, __ATOMIC_RELAXED, __ATOMIC_RELAXED );
  if (!suspended) {
    GENASSERT1(prevState == Running, FmtHex(prevStack));
    prevStack->resumeInternal();
  }
}

// destroy stack
void StackContext::postTerminate(StackContext* prevStack) {
  CHECK_PREEMPTION(0);
  prevStack->resumeProcessor->removeStack(_friend<StackContext>());
  Runtime::stackDestroy(prevStack);
}

// a new thread/stack starts in stubInit() and then jumps to this routine
extern "C" void invokeStack(funcvoid3_t func, ptr_t arg1, ptr_t arg2, ptr_t arg3) {
  CHECK_PREEMPTION(0);
  Runtime::EnablePreemption();
  func(arg1, arg2, arg3);
  Runtime::DisablePreemption();
  StackContext::terminate();
}

void StackContext::resumeInternal() {
  resumeProcessor->singleResume(*this, _friend<StackContext>());
}

bool StackContext::idleYield(_friend<BaseProcessor>) {
  CHECK_PREEMPTION(1);          // expect preemption still enabled
  Runtime::DisablePreemption();
  bool retcode = CurrStack()->switchStack<Idle>();
  Runtime::EnablePreemption();
  return retcode;
}

void StackContext::yield() {
  CHECK_PREEMPTION(1);          // expect preemption still enabled
  Runtime::DisablePreemption();
  CurrStack()->switchStack<Yield>();
  Runtime::EnablePreemption();
}

void StackContext::preempt() {
  CurrStack()->switchStack<Preempt>();
}

void StackContext::terminate() {
  CurrStack()->switchStack<Terminate>();
  unreachable();
}

void StackContext::suspend() {
  switchStack<Suspend>();
}

void StackContext::changeResumeProcessor(VirtualProcessor& rp, _friend<BaseProcessor>) {
  resumeProcessor->removeStack(_friend<StackContext>());
  resumeProcessor = &rp;
  resumeProcessor->addStack(_friend<StackContext>());
}

void StackContext::rebalance(size_t count) {
  StackContext* sc = CurrStack();
  if (sc->affinity || ++sc->balanceCounter % count != 0) return;
  sc->resumeProcessor->removeStack(_friend<StackContext>());
  sc->resumeProcessor = &CurrCluster().placement(_friend<StackContext>(), false, true);
  sc->resumeProcessor->addStack(_friend<StackContext>());
}

// migrate to cluster; adjust stackCounts, clear affinity
void StackContext::migrateSelf(Cluster& cluster) {
  StackContext* sc = CurrStack();
  sc->resumeProcessor->removeStack(_friend<StackContext>());
  sc->resumeProcessor = &cluster.placement(_friend<StackContext>(), false, true);
  sc->resumeProcessor->addStack(_friend<StackContext>());
  Runtime::DisablePreemption();
  sc->switchStack<Migrate>();
  Runtime::EnablePreemption();
}

// migrate to cluster (for disk I/O), don't change stackCount or affinity
VirtualProcessor& StackContext::migrateSelf(Cluster& cluster, _friend<EventScope>) {
  StackContext* sc = CurrStack();
  VirtualProcessor* proc = sc->resumeProcessor;
  sc->resumeProcessor = &cluster.placement(_friend<StackContext>(), false, true);
  Runtime::DisablePreemption();
  sc->switchStack<Migrate>();
  Runtime::EnablePreemption();
  return *proc;
}

// migrate back to previous processor (after disk I/O), don't change stackCount or affinity
void StackContext::migrateSelf(VirtualProcessor& proc, _friend<EventScope>) {
  StackContext* sc = CurrStack();
  sc->resumeProcessor = &proc;
  Runtime::DisablePreemption();
  sc->switchStack<Migrate>();
  Runtime::EnablePreemption();
}
