/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _BaseProcessor_h_
#define _BaseProcessor_h_ 1

#include "runtime/StackContext.h"

class BasePoller;
class Cluster;

class ReadyQueue {
  typedef SystemLock ReadyLock;
  ReadyLock  readyLock;
#if TESTING_CONCURRENT_READYQUEUE
  StackMPSC<> queue[numPriority];
#else
  StackQueue<> queue[numPriority];
#endif

  volatile size_t count;

  ReadyQueue(const ReadyQueue&) = delete;            // no copy
  ReadyQueue& operator=(const ReadyQueue&) = delete; // no assignment

public:
  template<bool AffinityCheck = false>
  StackContext* dequeue() {
#if TESTING_CONCURRENT_READYQUEUE
    while (count > 0) {                      // spin until push completes
      for (size_t p = 0; p <= maxPriority; p += 1) {
        StackContext* s = AffinityCheck ? queue[p].peek() : queue[p].pop();
        if (s) {
          if (AffinityCheck) {
            if (s->getAffinity()) return nullptr;
            else queue[p].pop<true>();
          }
          // just using '1' here seems to confuse clang without optimization
          __atomic_sub_fetch(&count, size_t(1), __ATOMIC_RELAXED);
          return s;
        }
      }
    }
#else
    ScopedLock<ReadyLock> sl(readyLock);
    for (size_t p = 0; p <= maxPriority; p += 1) {
      if (!queue[p].empty()) {
        if (AffinityCheck && queue[p].front()->getAffinity()) return nullptr;
        count -= 1;
        return queue[p].pop();
      }
    }
#endif
    return nullptr;
  }

  template<bool AffinityCheck = false>
  StackContext* dequeueSafe() {
#if TESTING_CONCURRENT_READYQUEUE
    ScopedLock<ReadyLock> sl(readyLock);
#endif
    return dequeue<AffinityCheck>();
  }

  bool singleEnqueue(StackContext& s) {
#if TESTING_CONCURRENT_READYQUEUE
    queue[s.getPriority()].push(s);
    return __atomic_add_fetch(&count, 1, __ATOMIC_RELAXED) == 1;
#else
    ScopedLock<ReadyLock> sl(readyLock);
    queue[s.getPriority()].push(s);
    count += 1;
    return count == 1;
#endif
  }

  bool bulkEnqueue(ResumeQueue& rq) {
#if TESTING_CONCURRENT_READYQUEUE
    bool retcode = (__atomic_fetch_add(&count, rq.count, __ATOMIC_RELAXED) == 0);
    for (size_t p = 0; p <= maxPriority; p += 1) {
      queue[p].transferAllFrom(rq.queue[p]);
    }
    return retcode;
#else
    ScopedLock<ReadyLock> sl(readyLock);
    for (size_t p = 0; p <= maxPriority; p += 1) {
      queue[p].transferAllFrom(rq.queue[p]);
    }
    count += rq.count;
    return count == rq.count;
#endif
  }

public:
  ReadyQueue() : count(0) {}
  size_t load() const { return count; }
};

class VirtualProcessor {
protected:
  ReadyQueue      readyQueue;
  Cluster&        cluster;
  volatile size_t stackCount;
  ProcessorStats* stats;

public:
  VirtualProcessor(Cluster& c, const char* n) : cluster(c), stackCount(0) {
    stats = new ProcessorStats(this, n);
  }

  size_t load() const { return readyQueue.load(); }

  virtual void wakeUp();
  void wakeCluster();

  void addStack(_friend<StackContext>) {
    __atomic_add_fetch(&stackCount, 1, __ATOMIC_RELAXED);
  }
  void removeStack(_friend<StackContext>) {
    __atomic_sub_fetch(&stackCount, 1, __ATOMIC_RELAXED);
  }

  void singleResume(StackContext& s, _friend<StackContext>) {
    GENASSERT1(s.getPriority() <= maxPriority, s.getPriority());
    Runtime::debugS("Stack ", FmtHex(&s), " queueing on ", FmtHex(this));
    stats->enq.count();
    if (readyQueue.singleEnqueue(s)) wakeUp();
    else wakeCluster();
  }

  // No locking for ResumeQueue! The caller better knows what it is doing.
  void bulkResume(ResumeQueue& rq, _friend<BasePoller>) {
    GENASSERT(rq.count);
    Runtime::debugS(rq.count, " stacks bulk queueing on ", FmtHex(this));
    stats->bulk.add(rq.count);
    if (readyQueue.bulkEnqueue(rq)) wakeUp();
    if (rq.count > 1) wakeCluster();
  }

  template<bool AffinityCheck = false>
  StackContext* dequeue(_friend<Cluster>) {
    return readyQueue.dequeueSafe<AffinityCheck>();
  }
};

class BaseProcessor;
#if TESTING_PLACEMENT_RR
typedef IntrusiveList<BaseProcessor,0,2> ProcessorList;
typedef IntrusiveRing<BaseProcessor,1,2> ProcessorRing;
#else
typedef IntrusiveList<BaseProcessor,0,1> ProcessorList;
#endif

class BaseProcessor : public ProcessorList::Link, public VirtualProcessor {
  inline bool tryLocal(StackContext*& s);
  inline bool tryStage(StackContext*& s);
  inline bool trySteal(StackContext*& s);
  inline bool tryBorrow(StackContext*& s);

protected:
  volatile bool terminate;
  StackContext* idleStack;
  void setupIdle(StackContext* is) {
    idleStack = is;
    idleStack->setPriority(idlePriority);
  }

  bool findWork();

public:
  BaseProcessor(Cluster& c);
  Cluster& getCluster() { return cluster; }
  bool empty() const { return stackCount == 1; } // only idle stack left
  StackContext* schedule(_friend<StackContext>);
};

#endif /* _BaseProcessor_h_ */
