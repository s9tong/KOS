/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#ifndef _Runtime_h_
#define _Runtime_h_ 1

#include "generic/basics.h"
#include "generic/stats.h"

class StackContext;

struct Runtime {
  /**** preemption enable/disable ****/
  static inline void DisablePreemption();
  static inline void EnablePreemption();

  /**** debug output ****/
  template<typename... Args> static inline void debugB(const Args&... a);
  template<typename... Args> static inline void debugS(const Args&... a);
  template<typename... Args> static inline void debugT(const Args&... a);

  /**** timer management ****/
  static inline void notifyTimeout(const Time& t);
  static inline Time now();

  /**** thread switch ****/
  static inline void noStackSwitch(StackContext* currStack);
  static inline void preStackSwitch(StackContext* currStack, StackContext* nextStack);
  static inline void postStackSwitch(StackContext* newStack);
  static inline void stackDestroy(StackContext* prevStack);
};

#if defined(__KOS__)

#include "kernel/Output.h"
#include "kernel/Clock.h"

#define CHECK_PREEMPTION(x) \
  GENASSERT1(LocalProcessor::checkLock() == (1-x), LocalProcessor::getLockCount())

typedef KernelLock      SystemLock;
typedef KernelLockRW    SystemLockRW;
typedef KernelProcessor SystemProcessor;

inline void Runtime::DisablePreemption() { LocalProcessor::lock(); }
inline void Runtime::EnablePreemption()  { LocalProcessor::unlock(true); }

inline void Runtime::notifyTimeout(const Time&) {}
inline Time Runtime::now() { return kernelClock.now(); }

template<typename... Args>
inline void Runtime::debugB(const Args&... a) { DBG::outl(DBG::Blocking, a...); }
template<typename... Args>
inline void Runtime::debugS(const Args&... a) { DBG::outl(DBG::Scheduling, a...); }
template<typename... Args>
inline void Runtime::debugT(const Args&... a) { DBG::outl(DBG::Threads, a...); }

#elif defined(__LIBFIBRE__)

#include "generic/ScopedLocks.h"
#include "generic/SpinLocks.h"
#include "libfibre/lfbasics.h"

#define CHECK_PREEMPTION(x)

inline void Runtime::DisablePreemption() {}
inline void Runtime::EnablePreemption()  {}

inline Time Runtime::now() {
  Time ct;
  SYSCALL(clock_gettime(CLOCK_REALTIME, &ct));
  return ct;
}

template<typename... Args>
inline void Runtime::debugB(const Args&... a) { dprintl(a...); }
template<typename... Args>
inline void Runtime::debugS(const Args&... a) { dprintl(a...); }
template<typename... Args>
inline void Runtime::debugT(const Args&... a) { dprintl(a...); }

#else
#error undefined platform: only __KOS__ or __LIBFIBRE__ supported at this time
#endif

#endif /* _Runtime_h_ */
