/******************************************************************************
    Copyright (C) Martin Karsten 2015-2018

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/
#include "runtime/Cluster.h"
#include "runtime/RuntimeImpl.h"

// from background & staging enqueue -> find idle "real" processor to run
void VirtualProcessor::wakeUp() {
  BaseProcessor* idleProc = cluster.findIdleProcessorHard();
  GENASSERT1(idleProc != this, FmtHex(this));
  if (idleProc) idleProc->wakeUp();
}

// any other enqueue -> wake idle processor, but don't try too hard
void VirtualProcessor::wakeCluster() {
#if TESTING_WAKE_CLUSTER
  BaseProcessor* idleProc = cluster.findIdleProcessorSoft();
  if (idleProc && idleProc != this) idleProc->wakeUp();
#endif
}

BaseProcessor::BaseProcessor(Cluster& c) : VirtualProcessor(c, "Processor"), terminate(false), idleStack(nullptr) {
  c.addProcessor(*this);
}

bool BaseProcessor::findWork() {
#if TESTING_IDLE_SPIN
  static const size_t spinMax = TESTING_IDLE_SPIN;
#else
  static const size_t spinMax = 1;
#endif
  for (size_t spin = 0; spin < spinMax; spin += 1) {
    if (StackContext::idleYield(_friend<BaseProcessor>())) return true;
    if (terminate) return true;
    Pause();
  }
  return false;
}

inline bool BaseProcessor::tryLocal(StackContext*& s) {
#if TESTING_WORK_STEALING
  s = readyQueue.dequeueSafe();
#else
  s = readyQueue.dequeue();
#endif
  if (s) {
    stats->deq.count();
    return true;
  }
  return false;
}

inline bool BaseProcessor::tryStage(StackContext*& s) {
  s = cluster.stage();
  if (s) {
    stats->stage.count();
    s->changeResumeProcessor(*this, _friend<BaseProcessor>());
    return true;
  }
  return false;
}

inline bool BaseProcessor::trySteal(StackContext*& s) {
#if TESTING_WORK_STEALING
  s = cluster.steal();
  if (s) {
#if TESTING_WORK_STEALING_STICKY
    static const size_t stickyStealThreshold = TESTING_WORK_STEALING_STICKY;
    if (s->getResumeProcessor().load() > stickyStealThreshold) {
      s->changeResumeProcessor(*this, _friend<BaseProcessor>());
      stats->move.count();
    } else
#endif
    stats->steal.count();
    return true;
  }
#endif
  return false;
}

inline bool BaseProcessor::tryBorrow(StackContext*& s) {
  s = cluster.borrow();
  if (s) {
    stats->borrow.count();
    return true;
  }
  return false;
}

StackContext* BaseProcessor::schedule(_friend<StackContext>) {
  StackContext* nextStack;
  if (tryLocal(nextStack)) return nextStack;
  if (terminate) return idleStack;
#if TESTING_POLLER_FIBRES
  if (tryBorrow(nextStack)) return nextStack;
  if (tryStage(nextStack)) return nextStack;
  if (trySteal(nextStack)) return nextStack;
#else
  if (tryStage(nextStack)) return nextStack;
  if (trySteal(nextStack)) return nextStack;
  if (tryBorrow(nextStack)) return nextStack;
#endif
  return idleStack;
}
